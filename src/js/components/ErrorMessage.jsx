const ErrorMessage = props => (
    <div className="row right-align">
        <div className="col s10 m8 offset-m2 offset-s1">
            <span className="red-text">{props.message}</span>
        </div>
    </div>
);

export default ErrorMessage;