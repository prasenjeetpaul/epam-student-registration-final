const AuthFooter = () => (
    <footer class="page-footer grey darken-3">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">EPAM Systems</h5>
                    <p class="grey-text text-lighten-4">We help customers around the world become competitive – and
                        stay competitive. We combine best-in-class software engineering with digital strategy and
                        experience design, business consulting and technology innovation services.</p>
                </div>
                <div class="col l6 s12 right-align">
                    <h5 class="white-text">Connect</h5>
                    <ul>
                        <li><a class="white-text" href="https://welcome.epam.in/" target="_blank">EPAM India</a></li>
                        {/* <!-- <li><a class="white-text" href="https://epam.com/" target="_blank">EPAM Global</a></li> --> */}
                        <li><a class="white-text" href="https://www.facebook.com/EPAM.India/" target="_blank">Facebook</a></li>
                        <li><a class="white-text" href="http://www.linkedin.com/company/4972?goback=.fcs_GLHD_EPAM_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2&amp;trk=ncsrch_hits"
                                target="_blank">LinkedIn</a></li>
                        <li><a class="white-text" href="mailto:epam-rdin_hiring@epam.com">Contact us</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright grey darken-4">
            <div class="container">
                &copy; <span class="brown-text text-lighten-3">2018 EPAM Systems, Inc. All Rights Reserved</span>
            </div>
        </div>
    </footer>
)

export default AuthFooter;