import { LoginAPI, RegisterAPI } from "../store/appState";

const AuthHeader = props => (
    <nav className="grey darken-3" role="navigation">
        <div className="nav-wrapper container">
            <div id="logo-container" className="brand-logo">
                <h5 className="light logo-text">EPAM STUDENT</h5>
            </div>

            {/* <!-- <img src="images/EPAM_LOGO.png" style="height: 60px;"/></a> --> */}
            <ul className="right hide-on-med-and-down">
                {/* <li><a onClick={props.linkAction}>{props.linkName}</a></li> */}
                <li><a href={ LoginAPI }>{props.linkName[0]}</a></li>
                <li><a href={ RegisterAPI }>{props.linkName[1]}</a></li>
            </ul>

            <ul id="nav-mobile" className="sidenav">
                {/* <li><a onClick={props.linkAction}>{props.linkName}</a></li> */}
                <li><a href={ LoginAPI }>{props.linkName[0]}</a></li>
                <li><a href={ RegisterAPI }>{props.linkName[1]}</a></li>
            </ul>
            <a href="#" data-target="nav-mobile" className="sidenav-trigger">
                <i class="material-icons">menu</i>
            </a>
        </div>
    </nav>
);

export default AuthHeader;