const Dropdown = props => (
    <select 
        className="browser-default" 
        onChange={event => props.onOptionSelect(event.target.value)} 
        required={props.isRequired}>
        <option value="" selected disabled>{props.defaultMessage}</option>
        {
            props.data.map(individualData => (
                <option 
                    key={individualData} 
                    selected={individualData==props.selectedValue}>
                    {individualData}
                </option>
            ))
        }
    </select>
);

export default Dropdown;