const Footer = () => (
    <footer className="page-footer grey darken-3 home-footer">
        <div className="footer-copyright grey darken-4">
            <div className="container">
            &copy; <a className="brown-text text-lighten-3">2018 EPAM Systems, Inc. All Rights Reserved</a>
            </div>
        </div>
    </footer>
);

export default Footer;