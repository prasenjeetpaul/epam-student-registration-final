const appState = {
    viewPath: "/",
    
    studentID: null,
    
    isLoggedIn: false,
    loginError: null,
    registerError: null,

    isPersonalFilled: false,
    isAddressFilled: false,
    isAcademicFilled: false,
    isCertificateFilled: false,
    isLocationFilled: false,
    isDocumentUploaded: false,

    personalDetailError: null,
    addressDetailError: null,
    academicDetailError: null,
    certificateDetailError: null,
    locationDetailError: null,
    documentDetailError: null,
}

export default appState;

// export const API = "http://localhost:8080";
export const API =  document.getElementById("contextPath").value;
export const LoginAPI = API + "/welcome";
export const RegisterAPI = API + "/registerNewUser";
export const captchaAPI = API + "/captcha";