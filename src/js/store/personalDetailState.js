const personalDetailState = {
    firstName: "",
    middleName: "",
    lastName: "",
    dateOfBirth: "",
    mobileNumber: "",
    gender: "",
}

export default personalDetailState;