const locationDetailState = {
    jobLocations: [],
    interviewLocations: [],
    
    jobLocationPreference: [],
    interviewLocationPreference: [],
}

export default locationDetailState;