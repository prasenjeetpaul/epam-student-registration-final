import { ACADEMIC_DETAIL_ACTION } from "../actions/actionType";
import academicDetailState from "../store/academicDetailState";

export default function(state = academicDetailState, action) {
    switch(action.type) {
        case ACADEMIC_DETAIL_ACTION.NAME_ENTERED: {
            return {
                ...state,
                name: [...action.payload]
            }
        }

        case ACADEMIC_DETAIL_ACTION.BOARD_ENTERED: {
            return {
                ...state,
                board: [...action.payload]
            };
        }
        
        case ACADEMIC_DETAIL_ACTION.GRADUATION_TYPE_ENTERED: {
            return {
                ...state,
                graduationType: [...action.payload]
            };
        }

        case ACADEMIC_DETAIL_ACTION.SPECIALIZATION_TYPE_ENTERED: {
            return {
                ...state,
                specializationType: [...action.payload]
            };
        }

        case ACADEMIC_DETAIL_ACTION.PASS_OUT_YEAR_ENTERED: {
            return {
                ...state,
                passOutYear: [...action.payload]
            }
        }

        case ACADEMIC_DETAIL_ACTION.PERCENTAGE_OF_MARKS_ENTERED: {
            return {
                ...state,
                percentageOfMarks: [...action.payload]
            }
        }

        case ACADEMIC_DETAIL_ACTION.IS_HIGHEST_EDUCATION_ENTERED: {
            return {
                ...state,
                isHighestEducation: [...action.payload]
            }
        }

        case ACADEMIC_DETAIL_ACTION.ACADEMIC_DATA_LOAD: {
            return {
                ...state,
                ...action.payload,
            }
        }

        default: {
            return state;
        }
    }
}