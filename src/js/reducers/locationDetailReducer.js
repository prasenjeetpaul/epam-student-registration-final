import { LOCATION_DETAIL_ACTION } from "../actions/actionType";
import locationDetailState from "../store/locationDetailState";

export default function(state = locationDetailState, action) {
    switch(action.type) {
        case LOCATION_DETAIL_ACTION.JOB_PREFERENCE_ENTERED: {
            return {
                ...state,
                jobLocationPreference: [...action.payload]
            }
        }

        case LOCATION_DETAIL_ACTION.INTERVIEW_PREFERENCE_ENTERED: {
            return {
                ...state,
                interviewLocationPreference: [...action.payload]
            };
        }

        case LOCATION_DETAIL_ACTION.JOB_LOCATION_LOADED: {
            return {
                ...state,
                jobLocations: [...action.payload]
            }
        }

        case LOCATION_DETAIL_ACTION.INTERVIEW_LOCATION_LOADED: {
            return {
                ...state,
                interviewLocations: [...action.payload]
            }
        }
        
        default: {
            return state;
        }
    }
}