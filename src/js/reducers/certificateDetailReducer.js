import { CERTIFICATE_DETAIL_ACTION } from "../actions/actionType";
import certificateDetailState from "../store/certificateDetailState";

export default function(state = certificateDetailState, action) {
    switch(action.type) {
        case CERTIFICATE_DETAIL_ACTION.NAME_ENTERED: {
            return {
                ...state,
                name: [...action.payload]
            }
        }

        case CERTIFICATE_DETAIL_ACTION.YEAR_ENTERED: {
            return {
                ...state,
                year: [...action.payload]
            };
        }

        case CERTIFICATE_DETAIL_ACTION.LINK_ENTERED: {
            return {
                ...state,
                link: [...action.payload]
            }
        }
        
        default: {
            return state;
        }
    }
}