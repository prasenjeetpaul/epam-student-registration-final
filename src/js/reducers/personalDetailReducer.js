import { PERSONAL_DETAIL_ACTION } from "../actions/actionType";
import personalDetailState from "../store/personalDetailState";

export default function(state = personalDetailState, action) {
    switch(action.type) {
        case PERSONAL_DETAIL_ACTION.FIRST_NAME_ENTERED: {
            return {
                ...state,
                firstName: action.payload
            }
        }
        
        case PERSONAL_DETAIL_ACTION.MIDDLE_NAME_ENTERED: {
            return {
                ...state,
                middleName: action.payload
            }
        }

        case PERSONAL_DETAIL_ACTION.LAST_NAME_ENTERED: {
            return {
                ...state,
                lastName: action.payload
            }
        }

        case PERSONAL_DETAIL_ACTION.DATE_OF_BIRTH_ENTERED: {
            return {
                ...state,
                dateOfBirth: action.payload
            }
        }

        case PERSONAL_DETAIL_ACTION.GENDER_ENTERED: {
            return {
                ...state,
                gender: action.payload
            }
        }

        case PERSONAL_DETAIL_ACTION.MOBILE_NUMBER_ENTERED: {
            return {
                ...state,
                mobileNumber: action.payload
            }
        }

        case PERSONAL_DETAIL_ACTION.PERSONAL_DATA_LOAD: {
            return {
                ...action.payload,
            }
        }

        default: {
            return state;
        }
    }
}