import { REGISTER_ACTION } from "./../actions/actionType";
import registerInitialState from "./../store/registerState";

export default function(state = registerInitialState, action) {
    switch(action.type) {
        case REGISTER_ACTION.EMAIL_ENTERED: {
            return {
                ...state,
                emailId: action.payload
            }
        }

        case REGISTER_ACTION.PASSWORD1_ENTERED: {
            return {
                ...state,
                password1: action.payload
            }
        }

        case REGISTER_ACTION.PASSWORD2_ENTERED: {
            return {
                ...state,
                password2: action.payload
            }
        }

        case REGISTER_ACTION.CAPTCHA_ENTERED: {
            return {
                ...state,
                captcha: action.payload
            }
        }

        default: {
            return state;
        }
    }
}