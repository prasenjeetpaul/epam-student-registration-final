import { APP_STATE_ACTION } from "./../actions/actionType";
import appState from "./../store/appState";

export default function(state = appState, action) {
    switch(action.type) {
        case APP_STATE_ACTION.PATH_CHANGED: {
            return {
                ...state,
                viewPath: action.payload
            }
        }
        case APP_STATE_ACTION.LOGIN_SUCCESS: {
            return {
                ...state,
                isLoggedIn: true
            }
        }

        case APP_STATE_ACTION.LOGIN_FAILED: {
            return {
                ...state,
                loginError: action.payload
            }
        }

        case APP_STATE_ACTION.REGISTER_FAILED: {
            return {
                ...state,
                registerError: action.payload
            }
        }

        case APP_STATE_ACTION.LOGOUT_CLICKED: {
            return {
                ...state,
                isLoggedIn: false,
                studentID: null,
            }
        }

        case APP_STATE_ACTION.REGISTER_SUCCESS: {
            return {
                ...state,
                isLoggedIn: true,
                studentID: action.payload
            }
        }

        /* Data Submit Case */
        case APP_STATE_ACTION.PEROSNAL_DATA_FILLED: {
            return {
                ...state,
                isPersonalFilled: action.payload
            }
        }

        case APP_STATE_ACTION.ADDRESS_DATA_FILLED: {
            return {
                ...state,
                isAddressFilled: action.payload
            }
        }

        case APP_STATE_ACTION.ACADEMIC_DATA_FILLED: {
            return {
                ...state,
                isAcademicFilled: action.payload
            }
        }

        case APP_STATE_ACTION.CERTIFICATE_DATA_FILLED: {
            return {
                ...state,
                isCertificateFilled: action.payload
            }
        }

        case APP_STATE_ACTION.LOCATION_DATA_FILLED: {
            return {
                ...state,
                isLocationFilled: action.payload
            }
        }

        case APP_STATE_ACTION.DOCUMENT_UPLOADED: {
            return {
                ...state,
                isDocumentUploaded: action.payload
            }
        }

        /* Errors Cases */
        case APP_STATE_ACTION.PERSONAL_DATA_SUBMIT_ERROR: {
            return {
                ...state,
                personalDetailError: action.payload.toString()
            }
        }

        case APP_STATE_ACTION.ADDRESS_DATA_SUBMIT_ERROR: {
            return {
                ...state,
                addressDetailError: action.payload.toString()
            }
        }

        case APP_STATE_ACTION.ACADEMIC_DATA_SUBMIT_ERROR: {
            return {
                ...state,
                academicDetailError: action.payload.toString()
            }
        }

        case APP_STATE_ACTION.CERTIFICATE_DATA_SUBMIT_ERROR: {
            return {
                ...state,
                certificateDetailError: action.payload.toString()
            }
        }

        case APP_STATE_ACTION.LOCATION_DATA_SUBMIT_ERROR: {
            return {
                ...state,
                locationDetailError: action.payload.toString()
            }
        }

        case APP_STATE_ACTION.DOCUMENT_DATA_SUBMIT_ERROR: {
            return {
                ...state,
                documentDetailError: action.payload.toString()
            }
        }

        default: {
            return state;
        }
    }
}