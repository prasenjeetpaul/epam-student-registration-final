import React from "react";
import { Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { changePath } from "../actions/appStateAction";
import Login from "./Login";
import Register from "./Register";
import AuthHeader from "../components/AuthHeader";
import AuthFooter from "../components/AuthFooter";

class AuthScreen extends React.Component {
    render() {
        return (
            <section>
                <AuthHeader 
                    linkName={["Login", "Register"]} 
                    linkAction={[
                        () => this.props.changePath("/login"),
                        () => this.props.changePath("/register")]}/>
                <main className="auth-background">
                    <div class="row auth-screen">
                        <div class="col s12 m6 l7 auth-banner">
                            <div class="auth-banner-content">
                                <img class="header center epam-logo-img"/>
                                <h3 class="header light black-text">
                                    Engineering the future
                                </h3>
                            </div>
                        </div>

                        {
                            this.props.viewPath === "/register"
                            ?   <Register />
                            :   <Register />
                        }

                        {
                            /* To be used with Routing..!
                            <section className="main-auth-content">
                                <Switch>
                                    <Route path="/login" component={Login} />
                                    <Route path="/register" component={Register} />
                                    <Route path="*" component={Login} />
                                </Switch>
                            </section> */
                        }
                    </div>
                </main>
                <AuthFooter />
            </section>
        )
    }
}
const mapDispatchToProps = dispatch => (
    bindActionCreators({
        changePath: changePath
    }, dispatch)
);

const mapStateToProps = state => ({
    viewPath: state.appState.viewPath,
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthScreen);