import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// import { Switch, Route} from "react-router-dom";
import Home from "./Home";
import AuthScreen from "./AuthScreen";
// import "../../css/materialize.css";
// import "../../css/materialize.min.css";
// import "../../css/style.css";
// import "../../css/admin.css";
import { saveUserID } from "../actions/registerAction";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = null;
        try {
            let userId = /*"813b0452-24cd-451e-ac2a-6b10521dcbca"//*/document.getElementById("userId").value;
            props.saveUserID(userId);
        } catch (e) {
        }
    }

    render() {
        if (this.props.studentID == null) {
            return <AuthScreen />
        }else if (this.props.viewPath === "/" || this.props.viewPath.substring(0, 5) === "/edit") {
            return <Home />
        } else {
            return <AuthScreen />
        }
        // To be used with routing..!
        // return (
        //     <Switch>
        //         <Route exact path="/" component={Home} />
        //         <Route path="/edit" component={Home} />
        //         <Route path="*" component={AuthScreen} />
        //     </Switch>
        // )
    }
}

const mapStateToProps = state => ({
    viewPath: state.appState.viewPath,
    studentID: state.appState.studentID,
})

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        saveUserID: saveUserID,
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(App);