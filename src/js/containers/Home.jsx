import React from "react";
import { connect } from "react-redux";
// import { Redirect, Switch, Link, Route} from "react-router-dom";
import { bindActionCreators } from "redux";
import { changePath } from "../actions/appStateAction";
import { logoutClicked } from "../actions/loginAction";
import ApplicationStatus from "./ApplicationStatus";
import PersonalDetail from "./PersonalDetail";
import AddressDetail from "./AddressDetail";
import AcademicDetail from "./AcademicDetail";
import DocumentDetail from "./DocumentDetail";
import CertificateDetail from "./CerificateDetail";
import LocationDetail from "./LocationDetail";
import Header from "../components/Header";
import Footer from "../components/Footer";
import { API } from "../store/appState";
import { fillPersonalDetails } from "../actions/personalDetailAction";
import { fillAddressDetails } from "../actions/addressDetailAction";
import { fillAcademicDetails } from "../actions/academicDetailAction";
import { fillCertificateDetails } from "../actions/certificateDetailAction";
import { fillLocationDetails } from "../actions/locationDetailAction"
import { loadDocumentStatus } from "../actions/documentDetailAction";

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.getView = this.getView.bind(this);
        this.logoutClicked = this.logoutClicked.bind(this);
        this.props.fillPersonalDetails();
        this.props.fillAddressDetails();
        this.props.fillAcademicDetails();
        this.props.fillCertificateDetails();
        this.props.fillLocationDetails();
        this.props.loadDocumentStatus();
    }

    getView(path) {
        if (path === "/edit/personal") {
            return <PersonalDetail />
        } else if (path === "/edit/address") {
            return <AddressDetail />
        } else if (path === "/edit/academic") {
            return <AcademicDetail />
        } else if (path === "/edit/certificate") {
            return <CertificateDetail />
        } else if (path === "/edit/location") {
            return <LocationDetail />
        } else if (path === "/edit/document") {
            return <DocumentDetail />
        } else if (path === "/edit/status") {
            return <ApplicationStatus/>
        } else if(!this.props.isPersonalFilled) {
            return <PersonalDetail/>
        } else {
            return <ApplicationStatus />
        }
    }

    logoutClicked() {
        this.props.logoutClicked();
        window.location = API + "/logout";
    }
    render() {
        let { changePath } = this.props;
        if (!this.props.isLoggedIn) {
            this.props.changePath("/login");
            return null;
            // return <Redirect to="/login" />
        } else {
            return (
                <section>
                    <Header sideLinkLabel="Logout" sideLinkOnClick={this.logoutClicked}/>
                    <div id="slide-out" className="sidenav sidenav-fixed grey darken-3 home-side-nav">
                        <div className="valign-wrapper center">
                            <img className="epam-logo epam-logo-img" alt="epam-logo" />
                        </div>
                        <h5>
                            <a  className="light grey-text text-lighten-5" 
                                onClick={() => changePath("/edit/personal")}>Personal Details</a>
                            { this.props.isPersonalFilled
                                ? <i className="material-icons green-text">check</i>
                                : null
                            }
                        </h5>
                        <h5>
                            <a  className="light grey-text text-lighten-5"
                                onClick={() => changePath("/edit/address")}>Address Details</a>
                            { this.props.isAddressFilled
                                ? <i className="material-icons green-text">check</i>
                                : null
                            }
                        </h5>
                        <h5>
                            <a  className="light grey-text text-lighten-5"
                                onClick={() => changePath("/edit/academic")}>Academic Details</a>
                            { this.props.isAcademicFilled
                                ? <i className="material-icons green-text">check</i>
                                : null
                            }
                        </h5>
                        <h5>
                            <a  className="light grey-text text-lighten-5"
                                onClick={() => changePath("/edit/certificate")}>Certification Details</a>
                            { this.props.isCertificateFilled
                                ? <i className="material-icons green-text">check</i>
                                : null
                            }
                        </h5>
                        <h5>
                            <a  className="light grey-text text-lighten-5"
                                onClick={() => changePath("/edit/location")}>Location Details</a>
                            { this.props.isLocationFilled
                                ? <i className="material-icons green-text">check</i>
                                : null
                            }
                        </h5>
                        <h5>
                            <a  className="light grey-text text-lighten-5"
                                onClick={() => changePath("/edit/document")}>Document Upload</a>
                            { this.props.isDocumentUploaded
                                ? <i className="material-icons green-text">check</i>
                                : null
                            }
                        </h5>
                        <h5>
                            <a  className="light grey-text text-lighten-5" 
                                onClick={() => changePath("/edit/status")}>Status</a>
                        </h5>
                        <h5>
                            <a  className="light grey-text text-lighten-5" 
                                onClick={this.logoutClicked}>Logout &nbsp;
                                <i className="material-icons logout-btn">exit_to_app</i>
                            </a>
                        </h5>

                    </div>
                    <main className="home-main">
                        {   this.getView(this.props.viewPath)   }
                    </main>

                    <Footer />




                </section>
            );
        }
    }
}

const mapStateToProps = state => ({
    viewPath: state.appState.viewPath,
    isLoggedIn: state.appState.isLoggedIn,
    
    isPersonalFilled: state.appState.isPersonalFilled,
    isAddressFilled: state.appState.isAddressFilled,
    isAcademicFilled: state.appState.isAcademicFilled,
    isCertificateFilled: state.appState.isCertificateFilled,
    isLocationFilled: state.appState.isLocationFilled,
    isDocumentUploaded: state.appState.isDocumentUploaded,
})

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        logoutClicked: logoutClicked,
        changePath: changePath,
        fillAddressDetails: fillAddressDetails,
        fillPersonalDetails: fillPersonalDetails,
        fillAcademicDetails: fillAcademicDetails,
        fillCertificateDetails: fillCertificateDetails,
        fillLocationDetails: fillLocationDetails,
        loadDocumentStatus: loadDocumentStatus,
    }, dispatch)
)
export default connect(mapStateToProps, mapDispatchToProps)(Home);