import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// import { Redirect, Link } from "react-router-dom";
import { registerEmailEntered, registerPassword1Entered, registerPassword2Entered, captchaEntered, registerClicked} from "../actions/registerAction";
import { changePath } from "../actions/appStateAction";
import ErrorMessage from "../components/ErrorMessage";
import { LoginAPI } from "../store/appState";
import { captchaAPI } from "../store/appState";

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.refreshCapcha =  this.refreshCapcha.bind(this);
        this.state = {
            captchaImage: captchaAPI
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.registerClicked();
    }

    refreshCapcha() {
        this.setState({
            captchaImage: captchaAPI + "?" + Math.random()
        })
        this.props.captchaEntered("");
    }

    render() {
        let toolTipStyle = {
            marginTop: "20px"
        }
        return(
            <div class="col s10 m5 l4 offset-s1">
            <div class="section card z-depth-5 black-text auth-form">
            <form onSubmit={this.handleSubmit}>

                <div className="row center">
                    <div className="col s12 m8 offset-m2">
                        <h4 className="header light">REGISTER</h4>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s10 m8 offset-m2 offset-s1">
                        <i className="material-icons prefix">email</i>
                        <input 
                            id="emailId" 
                            type="text"
                            className="validate" 
                            value={this.props.emailID}
                            onChange={event => this.props.registerEmailEntered(event.target.value)}
                            required/>
                        <label htmlFor="emailId">Email ID</label>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s9 m7 offset-m2 offset-s1">
                        <i className="material-icons prefix">lock</i>
                        <input 
                            id="password1" 
                            type="password" 
                            className="validate" 
                            value={this.props.password1}
                            onChange={event => this.props.registerPassword1Entered(event.target.value)}
                            required/>
                        <label htmlFor="password1">Password</label>
                    </div>
                    <div className="col s1 m1" style={toolTipStyle}>
                        <i  className="material-icons tooltipped" 
                            data-tooltip="Password must contain atleast one Upper-Case, Lower-Case, Number & a Special Character">info</i>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s10 m8 offset-m2 offset-s1">
                        <i className="material-icons prefix">lock</i>
                        <input 
                            id="password2" 
                            type="password" 
                            className="validate" 
                            value={this.props.password2}
                            onChange={event => this.props.registerPassword2Entered(event.target.value)}
                            required/>
                        <label htmlFor="password2">Confirm Password</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col s10 m8 offset-m2 offset-s1 right-align">
                        <img src={this.state.captchaImage} />
                        <a onClick={this.refreshCapcha}>
                            <i class="material-icons prefix">refresh</i>
                        </a>
                    </div>
                </div>
                
                <div class="row">
                    <div class="input-field col s10 m8 offset-m2 offset-s1">
                        <i class="material-icons prefix">vpn_key</i>
                        <input 
                            id="captcha" 
                            type="text" 
                            className="validate" 
                            value={this.props.captcha}
                            onChange={event => this.props.captchaEntered(event.target.value)}
                            required />
                        <label for="captcha">Verify Captcha</label>
                    </div>
                </div>

                {
                    this.props.registerError != null 
                    ? <ErrorMessage message={this.props.registerError}/>
                    : null
                }
                
                <div className="row center">
                    <div className="input-field col s10 m8 offset-m2 offset-s1">
                        <button className="btn waves-effect waves-light" type="submit" name="action">REGISTER
                        <i className="material-icons right">send</i>
                        </button>
                    </div>
                </div>

                <div className="row center">
                    <div className="input-field col s10 m8 offset-m2 offset-s1">
                        <span>
                            Already registered? 
                            {/* <a onClick={() => this.props.changePath("/login")}> Login now</a> */}
                            <a href={LoginAPI}> Login now</a>
                        </span>
                    </div>
                </div>
                
            </form>
            </div>
            </div>
        )





        /*
        if (this.props.isLoggedIn) {
            return <Redirect to="/"/>
        }
        return (
            <div className="auth-content">
                <form onSubmit={this.handleSubmit}>
                    <span className="auth-text">REGISTER</span>
                    <InputTag
                        type="text"
                        placeholder="Email"
                        value={this.props.emailID}
                        onChange={event => this.props.registerEmailEntered(event.target.value)} />
                    <InputTag
                        type="password"
                        placeholder="Password"
                        value={this.props.password1}
                        onChange={event => this.props.registerPassword1Entered(event.target.value)} />
                    <InputTag
                        type="password"
                        placeholder="Confirm Password"
                        value={this.props.password2}
                        onChange={event => this.props.registerPassword2Entered(event.target.value)} />
                    <Button
                        type="submit"
                        label="REGISTER" />
                </form>
                <div className="auth-helper">
                    {
                        this.props.registerError != null 
                        ? <ErrorMessage message={this.props.registerError}/>
                        : null
                    }
                    <span>
                        Already registered? <Link to="/login">Login now</Link>
                    </span>
                </div>
            </div>
        )*/
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        registerEmailEntered: registerEmailEntered,
        registerPassword1Entered: registerPassword1Entered,
        registerPassword2Entered: registerPassword2Entered,
        captchaEntered: captchaEntered,
        registerClicked: registerClicked,
        changePath: changePath
    }, dispatch)
);

const mapStateToProps = state => ({
    emailID: state.register.emailId,
    password1: state.register.password1,
    password2: state.register.password2,
    captcha: state.register.captcha,
    isLoggedIn: state.appState.isLoggedIn,
    registerError: state.appState.registerError
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);