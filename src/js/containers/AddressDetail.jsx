import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { AddressAction } from "../actions/addressDetailAction";
import { changePath } from "../actions/appStateAction";
import Spinner from "../components/Spinner";
import Dropdown from "../components/Dropdown";
import M from "materialize-css";
import ErrorMessage from "../components/ErrorMessage";


class AddressDetail extends React.Component {
    constructor(props) {
        super(props);
        this.nextClicked = this.nextClicked.bind(this);
        this.state = {
            spinnerClass: ["hide"],
        }
        // this.props.fetchCountry();
        this.props.fillAddressDetails();
    }

    static getDerivedStateFromProps(props, state) {
        if (props.addressDetailError!=null && props.addressDetailError!= "") {
            return {
                ...state,
                spinnerClass: ["hide"]
            }
        }
        return state
    }

    nextClicked(event) {
        event.preventDefault();
        this.props.addressDetailSubmitted(this.props.history);
        this.setState({
            spinnerClass: [""]
        })
    }

    componentDidUpdate() {
        M.updateTextFields();
    }

    render() {

        return (
            <div className="container  home-detail">
                <div className="section card z-depth-2 home-card hoverable">
        
                    <div className="row center">
                        <div className="col s12 m8 offset-m2">
                            <h4 className="header light">Address Details</h4>
                            <h6 className="header light">Enter your address details</h6>
                        </div>
                    </div>
                    <form onSubmit={this.nextClicked}>
                    
                    <div className="row">
                        <div className="input-field col s10 offset-s1 m8 offset-m2">
                            <select className="browser-default">
                            <option value="" disabled defaultValue>Address Type</option>
                            <option value="Permanent">Permanent</option>
                            {/* <option value="Temporary">Temporary</option> */}
                            </select>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s10 offset-s1 m8 offset-m2">
                            <i className="material-icons prefix">home</i>
                            <input 
                                id="houseNo" 
                                type="text" 
                                className="validate" 
                                value={this.props.houseNumber}
                                onChange={(event) => this.props.houseNumberEntered(event.target.value)}
                                required />
                            <label htmlFor="houseNo">House Number</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s10 offset-s1 m8 offset-m2">
                            <i className="material-icons prefix">subdirectory_arrow_right</i>
                            <input 
                                id="street" 
                                type="text" 
                                className="validate" 
                                value={this.props.street}
                                onChange={(event) => this.props.streetEntered(event.target.value)} required/>
                            <label htmlFor="street">Street</label>
                        </div>
                    </div>
                    
                    <div className="row">
                        <div className="input-field col s10 offset-s1 m8 offset-m2">
                            <i className="material-icons prefix">location_on</i>
                            <input 
                                id="landmark" 
                                type="text" 
                                className="validate" 
                                value={this.props.landmark}
                                onChange={(event) => this.props.landmarkEntered(event.target.value)} />
                            <label htmlFor="landmark">Landmark</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s10 offset-s1 m8 offset-m2">
                            <i className="material-icons prefix">location_city</i>
                            <input 
                                id="dateOfBirth" 
                                type="text" 
                                className="validate" 
                                value={this.props.city}
                                onChange={(event) => this.props.cityEntered(event.target.value)}
                                required />
                            <label htmlFor="city">City</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s10 offset-s1 m8 offset-m2">
                            <i className="material-icons prefix">gps_fixed</i>
                            <input 
                                id="zipcode" 
                                type="number" 
                                className="validate" 
                                value={this.props.zipcode}
                                onChange={(event) => this.props.zipcodeEntered(event.target.value)}
                                required />
                            <label htmlFor="zipcode">Zipcode</label>
                        </div>
                    </div>

                    {
                        this.props.country != null 
                        ?   <div className="row">
                            <div className="input-field col s10 offset-s1 m8 offset-m2">
                                <Dropdown 
                                    data={this.props.country}
                                    defaultMessage="Select Country"
                                    onOptionSelect={countryName => this.props.fetchState(countryName)}
                                    selectedValue={this.props.selectedCountry}
                                    isRequired={true}/>
                            </div>
                            </div>
                        :   null
                    }

                    {
                        this.props.state != null && this.props.state.length > 0
                        ?   <div className="row">
                            <div className="input-field col s10 offset-s1 m8 offset-m2">
                                <Dropdown 
                                    data={this.props.state}
                                    defaultMessage="Select State"
                                    onOptionSelect={stateName => this.props.selectedStateEntered(stateName)}
                                    selectedValue={this.props.selectedState}
                                    isRequired={true}
                                />
                            </div>
                            </div>
                        :   null
                    }
                    
                    {
                        this.props.addressDetailError != null
                        ? <ErrorMessage message={this.props.addressDetailError}/>
                        : null
                    }
                    
                    <div className="row center">
                        <Spinner className={this.state.spinnerClass.join(" ")} />
                    </div>
                    
                    <div className="row center">
                        <div className="input-field col s10 offset-s1 m4 offset-m2 left-align">
                            <button 
                                className="btn waves-effect waves-light" 
                                name="action"
                                onClick={() => this.props.changePath("/edit/personal")}>Prev
                            <i className="material-icons left">chevron_left</i>
                            </button>
                        </div>
                        <div className="col s1 show-on-small hide-on-med-and-up"></div>
                        <div className="input-field col s10 offset-s1 m4 right-align">
                            <button className="btn waves-effect waves-light" type="submit" name="action">Next
                            <i className="material-icons right">chevron_right</i>
                            </button>
                        </div>
                    </div>
                    </form>
                
                </div>

                {/* <div className="section">
                    <div className="fixed-action-btn">
                        <a className="btn-floating btn-large red">
                            <i className="large material-icons">add</i>
                        </a>
                    </div>
                </div> */}
            </div>
        )

    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        typeEntered: AddressAction.typeEntered,
        houseNumberEntered: AddressAction.houseNumberEntered,
        streetEntered: AddressAction.streetEntered,
        landmarkEntered: AddressAction.landmarkEntered,
        cityEntered: AddressAction.cityEntered,
        zipcodeEntered: AddressAction.zipcodeEntered,
        addressDetailSubmitted: AddressAction.addressDetailSubmitted,

        fetchCountry: AddressAction.fetchCountry,
        changePath: changePath,
        fillAddressDetails: AddressAction.fillAddressDetails,

        fetchState: AddressAction.fetchState,
        selectedStateEntered: AddressAction.selectedStateEntered,
        selectedCountryEntered: AddressAction.selectedCountryEntered,

    }, dispatch)
);

const mapStateToProps = state => ({
    type: state.addressDetail.type,
    houseNumber: state.addressDetail.houseNumber,
    street: state.addressDetail.street,
    landmark: state.addressDetail.landmark,
    city: state.addressDetail.city,
    state: state.addressDetail.state,
    zipcode: state.addressDetail.zipcode,
    country: state.addressDetail.country,
    addressDetailError: state.appState.addressDetailError,

    selectedState: state.addressDetail.selectedState,
    selectedCountry: state.addressDetail.selectedCountry,
});

export default connect(mapStateToProps, mapDispatchToProps)(AddressDetail);