import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { LocationAction } from "../actions/locationDetailAction";
import { changePath } from "../actions/appStateAction";
import Spinner from "../components/Spinner";
import Dropdown from "../components/Dropdown";
import ErrorMessage from "../components/ErrorMessage";

class LocationDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            spinnerClass: ["hide"]
        }
        this.styles = {
            margin: "20px",
            paddingTop: "10px",
        };
        this.props.loadLocationData();
        this.nextClicked = this.nextClicked.bind(this);
    }

    nextClicked(event) {
        event.preventDefault();
        this.props.locationDetailSubmitted();
        this.setState({
            spinnerClass: [""]
        })
    } 


    static getDerivedStateFromProps(props, state) {
        if (props.locationDetailError != null && props.locationDetailError != "") {
            return {
                ...state,
                spinnerClass: ["hide"]
            }
        }
        return state
    }

    render() {
        return (
            <div className="container home-detail">
                <div className="section card z-depth-2 home-card hoverable">

                    <div className="row center">
                        <div className="col s12 m8 offset-m2">
                            <h4 className="header light">Location Details</h4>
                            <h6 className="header light">Enter your location preferences<br/>(Optional)</h6>
                        </div>
                    </div>

                    <form onSubmit={this.nextClicked}>
                        <div style={this.styles}>
                            <div className="row">
                                <div className="col s3 m2 offset-m2" style={{paddingTop: "25px"}}>
                                    <h6>Job Location:</h6>
                                </div>
                                <div className="input-field col s3 m2 center">
                                    <span>Preference 1</span>
                                    <Dropdown
                                        data={this.props.jobLocations}
                                        defaultMessage="Location"
                                        onOptionSelect={locationName => this.props.jobPreferenceEntered(locationName, 0)}
                                        selectedValue={this.props.jobLocationPreference[0]}/>
                                </div>
                                <div className="input-field col s3 m2 center">
                                    <span>Preference 2</span>
                                    <Dropdown
                                        data={this.props.jobLocations}
                                        defaultMessage="Location"
                                        onOptionSelect={locationName => this.props.jobPreferenceEntered(locationName, 1)}
                                        selectedValue={this.props.jobLocationPreference[1]}
                                        isRequired={false} />
                                </div>
                                <div className="input-field col s3 m2 center">
                                    <span>Preference 3</span>
                                    <Dropdown
                                        data={this.props.jobLocations}
                                        defaultMessage="Location"
                                        onOptionSelect={locationName => this.props.jobPreferenceEntered(locationName, 2)}
                                        selectedValue={this.props.jobLocationPreference[2]}
                                        isRequired={false}/>
                                </div>
                            </div>
                        </div>


                        <div style={this.styles}>
                            <div className="row">
                                <div className="col s3 m2 offset-m2" style={{paddingTop: "20px"}}>
                                    <h6>Interview Location:</h6>
                                </div>
                                <div className="input-field col s3 m2 center">
                                    <span>Preference 1</span>
                                    <Dropdown
                                        data={this.props.interviewLocations}
                                        defaultMessage="Location"
                                        onOptionSelect={locationName => this.props.interviewPreferenceEntered(locationName, 0)}
                                        selectedValue={this.props.interviewLocationPreference[0]}
                                        isRequired={false} />
                                </div>
                                <div className="input-field col s3 m2 center">
                                    <span>Preference 2</span>
                                    <Dropdown
                                        data={this.props.interviewLocations}
                                        defaultMessage="Location"
                                        onOptionSelect={locationName => this.props.interviewPreferenceEntered(locationName, 1)}
                                        selectedValue={this.props.interviewLocationPreference[1]}
                                        isRequired={false} />
                                </div>
                                <div className="input-field col s3 m2 center">
                                    <span>Preference 3</span>
                                    <Dropdown
                                        data={this.props.interviewLocations}
                                        defaultMessage="Location"
                                        onOptionSelect={locationName => this.props.interviewPreferenceEntered(locationName, 2)}
                                        selectedValue={this.props.interviewLocationPreference[2]}
                                        isRequired={false} />
                                </div>
                            </div>
                        </div>

                        {
                            this.props.locationDetailError != null
                            ? <ErrorMessage message={this.props.locationDetailError}/>
                            : null
                        }
                        
                        <div className="row center">
                            <Spinner className={this.state.spinnerClass.join(" ")} />
                        </div>

                        <div className="row center">
                            <div className="input-field col s10 offset-s1 m4 offset-m2 left-align">
                                <button
                                    className="btn waves-effect waves-light"
                                    name="action"
                                    onClick={() => this.props.changePath("/edit/certificate")}>Prev
                                        <i className="material-icons left">chevron_left</i>
                                </button>
                            </div>
                            <div className="col s1 show-on-small hide-on-med-and-up"></div>
                            <div className="input-field col s10 offset-s1 m4 right-align">
                                <button className="btn waves-effect waves-light" type="submit" name="action">Next
                                    <i className="material-icons right">chevron_right</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => (
    bindActionCreators({
        changePath: changePath,
        loadLocationData: LocationAction.loadLocationData,
        jobPreferenceEntered: LocationAction.jobPreferenceEntered,
        interviewPreferenceEntered: LocationAction.interviewPreferenceEntered,
        locationDetailSubmitted: LocationAction.locationDetailSubmitted,
    }, dispatch)
);

const mapStateToProps = state => ({
    jobLocations: state.locationDetail.jobLocations,
    interviewLocations: state.locationDetail.interviewLocations,
    jobLocationPreference: state.locationDetail.jobLocationPreference,
    interviewLocationPreference: state.locationDetail.interviewLocationPreference,
    locationDetailError: state.appState.locationDetailError,
});

export default connect(mapStateToProps, mapDispatchToProps)(LocationDetail);