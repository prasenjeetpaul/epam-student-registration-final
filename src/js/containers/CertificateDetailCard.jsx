import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { CertificateAction } from "../actions/certificateDetailAction";
import { changePath } from "../actions/appStateAction";
import M from "materialize-css";

class CertificateDetailCard extends React.Component {
    constructor(props) {
        super(props);
        this.styles = {
            margin: "20px",
        };
    }
    render() {
        let { id } = this.props;
        return (
            <div style={this.styles} className="card-panel">
                
                {
                    !this.props.showDelete
                    ?   null
                    :   <div className="row right-align">
                            <div className="input-field col s12">
                                <a  className="btn-floating btn-large waves-effect waves-light red"
                                    onClick={this.props.deleteAction}>
                                    <i className="material-icons">delete</i>
                                </a>
                            </div>
                        </div>
                }
                

                <div className="row">
                    <div className="input-field col s12 m10 offset-m1">
                        <i className="material-icons prefix">web</i>
                        <input 
                            id={"certificateName" + id} 
                            type="text" 
                            className="validate" 
                            value={this.props.name[id]}
                            onChange={event => this.props.nameEntered(event.target.value, id)} 
                            />
                        <label htmlFor={"certificateName" + id}>Certification Name</label>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s12 m10 offset-m1">
                        <i className="material-icons prefix">date_range</i>
                        <input 
                            id={"certificateYear" + id}
                            type="number" 
                            className="validate" 
                            value={this.props.year[id]}
                            onChange={event => this.props.yearEntered(event.target.value, id)} 
                            />
                        <label htmlFor={"certificateYear" + id}>Certification Year</label>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s12 m10 offset-m1">
                        <i className="material-icons prefix">insert_link</i>
                        <input 
                            id={"certificateLink" + id}
                            type="text" 
                            className="validate" 
                            value={this.props.link[id]}
                            onChange={event => this.props.linkEntered(event.target.value, id)} 
                            />
                        <label htmlFor={"certificateLink" + id}>Certification Link</label>
                    </div>
                </div>

                {M.updateTextFields()}
            </div>
            
        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        nameEntered: CertificateAction.nameEntered,
        yearEntered: CertificateAction.yearEntered,
        linkEntered: CertificateAction.linkEntered,
    }, dispatch)
);

const mapStateToProps = state => ({
    name: state.certificateDetail.name,
    year: state.certificateDetail.year,
    link: state.certificateDetail.link,
});

export default connect(mapStateToProps, mapDispatchToProps)(CertificateDetailCard);