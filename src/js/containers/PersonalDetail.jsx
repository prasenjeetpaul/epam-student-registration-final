import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { 
    firstNameEntered, 
    middleNameEntered, 
    lastNameEntered, 
    dateOfBirthEntered, 
    mobileNumberEntered, 
    personalDetailSubmitted,
    fillPersonalDetails,
    genderEntered } from "../actions/personalDetailAction";
import { changePath } from "../actions/appStateAction";
import Dropdown from "../components/Dropdown";
import Spinner from "../components/Spinner";
import ErrorMessage from "../components/ErrorMessage";

class PersonalDetail extends React.Component {
    constructor(props) {
        super(props);
        this.nextClicked = this.nextClicked.bind(this);
        this.state = {
            spinnerClass: ["hide"]
        }
        this.props.fillPersonalDetails();
    }

    static getDerivedStateFromProps(props, state) {
        if (props.personalDetailError!=null && props.personalDetailError!= "") {
            return {
                ...state,
                spinnerClass: ["hide"]
            }
        }
        return state;
    }

    nextClicked(event) {
        event.preventDefault();
        this.props.personalDetailSubmitted(this.props.history);
        this.setState({
            spinnerClass: [""]
        })
    }

    render() {
        return (
            <div className="container home-detail">
                <div className="section card z-depth-2 home-card hoverable">
        
                    <div className="row center">
                        <div className="col s12 m8 offset-m2">
                            <h4 className="header light">Personal Details</h4>
                            <h6 className="header light">Enter your personal details</h6>
                        </div>
                    </div>
                    <form onSubmit={this.nextClicked}>
                    <div className="row">
                        <div className="input-field col s10 offset-s1 m8 offset-m2">
                            <i className="material-icons prefix">person</i>
                            <input 
                                id="firstName" 
                                type="text" 
                                className="validate" 
                                value={this.props.firstName}
                                onChange={(event) => this.props.firstNameEntered(event.target.value)}
                                required />
                            <label htmlFor="firstName">First Name</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s10 offset-s1 m8 offset-m2">
                            <i className="material-icons prefix">person</i>
                            <input 
                                id="middleName" 
                                type="text" 
                                className="validate" 
                                value={this.props.middleName}
                                onChange={(event) => this.props.middleNameEntered(event.target.value)}/>
                            <label htmlFor="middleName">Middle Name</label>
                        </div>
                    </div>
                    
                    <div className="row">
                        <div className="input-field col s10 offset-s1 m8 offset-m2">
                            <i className="material-icons prefix">person</i>
                            <input 
                                id="lastName" 
                                type="text" 
                                className="validate" 
                                value={this.props.lastName}
                                onChange={(event) => this.props.lastNameEntered(event.target.value)}
                                required />
                            <label htmlFor="lastName">Last Name</label>
                        </div>
                    </div>

                    <div className="row">
                        <i className="col s1 offset-s1 m1 offset-m2 material-icons prefix" style={{paddingTop: "20px"}}>person</i>
                        <div className="input-field col s9 m7">
                            <Dropdown
                                data={["Male", "Female", "Other"]}
                                defaultMessage="Gender"
                                onOptionSelect={selectedGender => this.props.genderEntered(selectedGender)}
                                selectedValue={this.props.gender}
                                isRequired={true}/>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s10 offset-s1 m8 offset-m2">
                            <i className="material-icons prefix">date_range</i>
                            <input 
                                id="dateOfBirth" 
                                type="date" 
                                value={this.props.dateOfBirth}
                                onChange={(event) => this.props.dateOfBirthEntered(event.target.value)}
                                required />
                            <label htmlFor="dateOfBirth">Date of Birth</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s10 offset-s1 m8 offset-m2">
                            <i className="material-icons prefix">call</i>
                            <input 
                                id="mobileNumber" 
                                type="number" 
                                className="validate" 
                                value={this.props.mobileNumber}
                                onChange={(event) => this.props.mobileNumberEntered(event.target.value)}
                                required />
                            <label htmlFor="mobileNumber">Phone Number</label>
                        </div>
                    </div>

                    {
                        this.props.personalDetailError != null
                        ? <ErrorMessage message={this.props.personalDetailError}/>
                        : null
                    }
                    <div className="row center">
                        <Spinner className={this.state.spinnerClass.join(" ")} />
                    </div>
                    
                    <div className="row center">
                        <div className="input-field col s10 offset-s1 m4 offset-m2 left-align">
                            {/* <button 
                                className="btn waves-effect waves-light" 
                                onClick={() => this.props.changePath("/edit/personal")}>Prev
                            <i className="material-icons left">chevron_left</i>
                            </button> */}
                        </div>
                        <div className="col s1 show-on-small hide-on-med-and-up"></div>
                        <div className="input-field col s10 offset-s1 m4 right-align">
                            <button className="btn waves-effect waves-light" type="submit" name="action">Next
                            <i className="material-icons right">chevron_right</i>
                            </button>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        firstNameEntered: firstNameEntered,
        middleNameEntered: middleNameEntered,
        lastNameEntered: lastNameEntered,
        dateOfBirthEntered: dateOfBirthEntered,
        mobileNumberEntered: mobileNumberEntered,
        personalDetailSubmitted: personalDetailSubmitted,
        genderEntered: genderEntered,

        changePath: changePath,
        fillPersonalDetails: fillPersonalDetails,
    }, dispatch)
);

const mapStateToProps = state => ({
    firstName: state.personalDetail.firstName,
    middleName: state.personalDetail.middleName,
    lastName: state.personalDetail.lastName,
    gender: state.personalDetail.gender,
    dateOfBirth: state.personalDetail.dateOfBirth,
    mobileNumber: state.personalDetail.mobileNumber,
    personalDetailError: state.appState.personalDetailError,

});

export default connect(mapStateToProps, mapDispatchToProps)(PersonalDetail);