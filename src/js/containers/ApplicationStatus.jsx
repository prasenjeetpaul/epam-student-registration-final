import React from "react";
import { connect } from "react-redux";
import axios from "axios";
import { API } from "./../store/appState";

class ApplicationStatus extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            statusDate: null,
        }
        let statusAPI = API + "/user/secure/getRegistrationEndDate";
        axios.get(statusAPI).then(response => this.setState({
            statusDate: response.data
        }))
    }
    render() {
        let status = <h5 className="header light">
            To know the status of your application, you can come back here by <b>{this.state.statusDate}</b> and check your status
        </h5>
        if (!(this.props.isPersonalFilled && this.props.isAcademicFilled && this.props.isAddressFilled && this.props.isDocumentUploaded)) {
            status = <h5 className="header light">
                Please complete your profile to view the application status
            </h5>
        }
        return (
            <div className="container home-detail ">
                <div className="section card z-depth-2 home-card hoverable">
                    <div className="row center">
                        <div className="col s10 offset-s1 m8 offset-m2">
                            {status}
                        </div>
                    </div>
                
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    isPersonalFilled: state.appState.isPersonalFilled,
    isAddressFilled: state.appState.isAddressFilled,
    isAcademicFilled: state.appState.isAcademicFilled,
    isDocumentUploaded: state.appState.isDocumentUploaded,
})

export default connect(mapStateToProps)(ApplicationStatus);