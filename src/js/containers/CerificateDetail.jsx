import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { CertificateAction } from "../actions/certificateDetailAction";
import { changePath } from "../actions/appStateAction";
import Spinner from "../components/Spinner";
import CertificateDetailCard from "./CertificateDetailCard";
import M from "materialize-css";
import ErrorMessage from "../components/ErrorMessage";


class CertificateDetail extends React.Component {
    constructor(props) {
        super(props);
        this.nextClicked = this.nextClicked.bind(this);
        this.state = {
            spinnerClass: ["hide"],
            detailCount: 1,
            maxDetailCount: 5,
        }
        this.addCard = this.addCard.bind(this);
        this.detailCard = [];
        this.deleteCard = this.deleteCard.bind(this);
        //this.props.fillAcademicDetails();
    }

    static getDerivedStateFromProps(props, state) {
        if (props.certificateDetailError!=null && props.certificateDetailError!= "") {
            return {
                ...state,
                spinnerClass: ["hide"]
            }
        }
        return state
    }

    addCard() {
        if (this.state.detailCount >= this.state.maxDetailCount) {
            M.toast({html: 'Cannot add more than 5 certificate records', classes: 'rounded'});
            return;
        }
        this.setState({
            detailCount: this.state.detailCount + 1,
        })
    }

    deleteCard(id) {
        this.props.deleteRecord(id); //To remove data from store
        this.detailCard.splice( id, 1 );
        this.setState({
            detailCount: this.state.detailCount - 1,
        })
    }

    nextClicked(event) {
        event.preventDefault();
        this.props.certificateDetailSubmitted();
        this.setState({
            spinnerClass: [""]
        })
    }

    

    render() {
        this.detailCard = [];
        let max = this.state.detailCount > this.props.name.length ? this.state.detailCount : this.props.name.length;
        if (max != this.state.detailCount) {
            this.setState({
                detailCount: this.props.name.length
            })
        }
        for (let i = 0 ; i < max; i++) {
            if (i > this.state.maxDetailCount) {
                M.toast({html: 'Cannot add more than 5 academic records', classes: 'rounded'});
                break;
            }
            if (i==0 || i < this.state.detailCount-1) {
                this.detailCard.push(<CertificateDetailCard 
                    key={i} 
                    id={i} 
                    showDelete={false}/>);
            } else {
                this.detailCard.push(<CertificateDetailCard 
                    key={i} 
                    id={i} 
                    showDelete={true}
                    deleteAction={() => this.deleteCard(i)}/>);
            }
            M.updateTextFields();
        }
        

        return(
            <div className="container home-detail">
                <div className="section card z-depth-2 home-card hoverable">
        
                    <div className="row center">
                        <div className="col s12 m8 offset-m2">
                            <h4 className="header light">Certification Details</h4>
                            <h6 className="header light">Enter your achievements<br/>(Optional)</h6>
                        </div>
                    </div>
                    <form onSubmit={this.nextClicked}>

                    <div className="row center">
                        <div className="col s10 offset-s1 m10 offset-m1">
                        {
                            this.detailCard
                        }
                        </div>
                    </div>


                    {
                        //To be changed
                        this.props.certificateDetailError != null
                        ? <ErrorMessage message={this.props.certificateDetailError}/>
                        : null
                    }

                    <div className="row center">
                        <Spinner className={this.state.spinnerClass.join(" ")} />
                    </div>
                    
                    
                    <div className="row">
                        <div className="col s10 offset-s1 m4 offset-m6 right-align">
                            <span>
                                Add more records &nbsp;&nbsp;
                            </span>
                            <a className="btn-floating btn-small green" onClick={this.addCard}>
                                <i className="material-icons tooltipped">add</i>
                            </a>
                        </div>
                    </div>

                    <div className="row center">
                        <div className="input-field col s10 offset-s1 m4 offset-m2 left-align">
                            <button 
                                className="btn waves-effect waves-light" 
                                name="action"
                                onClick={() => this.props.changePath("/edit/academic")}>Prev
                            <i className="material-icons left">chevron_left</i>
                            </button>
                        </div>
                        <div className="col s1 show-on-small hide-on-med-and-up"></div>
                        <div className="input-field col s10 offset-s1 m4 right-align">
                            <button className="btn waves-effect waves-light" type="submit" name="action">Next
                            <i className="material-icons right">chevron_right</i>
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
                {M.updateTextFields()}
            </div> 
        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        certificateDetailSubmitted: CertificateAction.certificateDetailSubmitted,
        deleteRecord: CertificateAction.deleteRecord,
        changePath: changePath,
        // fillAcademicDetails: fillAcademicDetails,
    }, dispatch)
);

const mapStateToProps = state => ({
    name: state.certificateDetail.name,
    year: state.certificateDetail.year,
    link: state.certificateDetail.link,
    certificateDetailError: state.appState.certificateDetailError,
});

export default connect(mapStateToProps, mapDispatchToProps)(CertificateDetail);