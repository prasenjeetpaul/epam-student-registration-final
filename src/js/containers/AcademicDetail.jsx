import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { Academic_Action } from "../actions/academicDetailAction";
import { changePath } from "../actions/appStateAction";
import { fillAcademicDetails } from "../actions/academicDetailAction";
import Spinner from "../components/Spinner";
import AcademicDetailCard from "./AcademicDetailCard";
import M from "materialize-css";
import ErrorMessage from "../components/ErrorMessage";


class AcademicDetail extends React.Component {
    constructor(props) {
        super(props);
        this.nextClicked = this.nextClicked.bind(this);
        this.state = {
            spinnerClass: ["hide"],
            detailCount: 1,
            maxDetailCount: 5,
        }
        this.addCard = this.addCard.bind(this);
        this.detailCard = [];
        this.deleteCard = this.deleteCard.bind(this);
        //this.props.fillAcademicDetails();
    }

    static getDerivedStateFromProps(props, state) {
        if (props.academicDetailError!=null && props.academicDetailError!= "") {
            return {
                ...state,
                spinnerClass: ["hide"]
            }
        }
        return state
    }

    addCard() {
        if (this.state.detailCount >= this.state.maxDetailCount) {
            M.toast({html: 'Cannot add more than 5 academic records', classes: 'rounded'});
            return;
        }
        this.setState({
            detailCount: this.state.detailCount + 1,
        })
    }

    deleteCard(id) {
        this.props.deleteRecord(id);
        this.detailCard.splice( id, 1 );
        this.setState({
            detailCount: this.state.detailCount - 1,
        })
    }

    nextClicked(event) {
        event.preventDefault();
        let highEduArray = this.props.isHighestEducation;
        if (!highEduArray.includes("true")) {
            M.toast({html: 'Atleast one highest eductaion is required', classes: 'rounded'});
            return;
        } else {
            var highCount = 0;
            for(var i = 0; i < highEduArray.length; i++){
                if(highEduArray[i] == "true")
                highCount++;
                if (highCount > 1) {
                    M.toast({html: 'Only one highest eductaion is allowed', classes: 'rounded'});
                    return;
                }
            }
        }
        this.props.academicDetailSubmitted(this.props.history);
        this.setState({
            spinnerClass: [""]
        })
    }

    

    render() {
        this.detailCard = [];
        let max = this.state.detailCount > this.props.name.length ? this.state.detailCount : this.props.name.length;
        if (max != this.state.detailCount) {
            this.setState({
                detailCount: this.props.name.length
            })
        }
        for (let i = 0 ; i < max; i++) {
            if (i > this.state.maxDetailCount) {
                M.toast({html: 'Cannot add more than 5 academic records', classes: 'rounded'});
                break;
            }
            if (i==0 || i < this.state.detailCount-1) {
                this.detailCard.push(<AcademicDetailCard 
                    key={i} 
                    id={i} 
                    showDelete={false}/>);
            } else {
                this.detailCard.push(<AcademicDetailCard 
                    key={i} 
                    id={i} 
                    showDelete={true}
                    deleteAction={() => this.deleteCard(i)}/>);
            }
            M.updateTextFields();
        }
        

        return(
            <div className="container  home-detail">
                <div className="section card z-depth-2 home-card hoverable">
        
                    <div className="row center">
                        <div className="col s12 m8 offset-m2">
                            <h4 className="header light">Academic Details</h4>
                            <h6 className="header light">Enter your academic details</h6>
                        </div>
                    </div>
                    <form onSubmit={this.nextClicked}>
                    
                    {/* <div className="row">
                        <div className="input-field  col s10 offset-s1 m8 offset-m2">
                            <select class="browser-default">
                            <option value="" disabled selected>Address Type</option>
                            <option value="Permanent">Permanent</option>
                            <option value="Temporary">Temporary</option>
                            </select>
                        </div>
                    </div> */}


                    <div className="row">
                        <div className="col s10 offset-s1 m10 offset-m1">
                        {
                            this.detailCard
                        }
                        </div>
                    </div>

                    {
                        this.props.academicDetailError != null
                        ? <ErrorMessage message={this.props.academicDetailError}/>
                        : null
                    }

                    <div className="row center">
                        <Spinner className={this.state.spinnerClass.join(" ")} />
                    </div>
                    
                    
                    <div className="row">
                        <div className="col s10 offset-s1 m4 offset-m6 right-align">
                            <span>
                                Add more records &nbsp;&nbsp;
                            </span>
                            <a className="btn-floating btn-small green" onClick={this.addCard}>
                                <i className="material-icons tooltipped">add</i>
                            </a>
                        </div>
                    </div>

                    <div className="row center">
                        <div className="input-field col s10 offset-s1 m4 offset-m2 left-align">
                            <button 
                                className="btn waves-effect waves-light" 
                                name="action"
                                onClick={() => this.props.changePath("/edit/address")}>Prev
                            <i className="material-icons left">chevron_left</i>
                            </button>
                        </div>
                        <div className="col s1 show-on-small hide-on-med-and-up"></div>
                        <div className="input-field col s10 offset-s1 m4 right-align">
                            <button className="btn waves-effect waves-light" type="submit" name="action">Next
                            <i className="material-icons right">chevron_right</i>
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
                {M.updateTextFields()}
            </div> 
        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        nameEntered: Academic_Action.nameEntered,
        boardEntered: Academic_Action.boardEntered,
        passOutYearEntered: Academic_Action.passOutYearEntered,
        percentageOfMarksEntered: Academic_Action.percentageOfMarksEntered,
        isHighestEducationEntered: Academic_Action.isHighestEducationEntered,
        academicDetailSubmitted: Academic_Action.academicDetailSubmitted,
        
        changePath: changePath,
        deleteRecord: Academic_Action.deleteRecord,
        fillAcademicDetails: fillAcademicDetails,
    }, dispatch)
);

const mapStateToProps = state => ({
    name: state.academicDetail.name,
    board: state.academicDetail.board,
    passOutYear: state.academicDetail.passOutYear,
    percentageOfMarks: state.academicDetail.percentageOfMarks,
    isHighestEducation: state.academicDetail.isHighestEducation,
    academicDetailError: state.appState.academicDetailError,
});

export default connect(mapStateToProps, mapDispatchToProps)(AcademicDetail);