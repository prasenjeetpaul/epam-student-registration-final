import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { DocumentAction } from "../actions/documentDetailAction";
import Spinner from "../components/Spinner";
import { changePath } from "../actions/appStateAction";
import M from "materialize-css";
import ErrorMessage from "../components/ErrorMessage";


class DocumentDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            spinnerClass: ["hide"],
            termClass: ["hide"],
            termsVisible: false,
            isTermsAccepted: props.isDocumentUploaded
        }
        this.termStyle = {
            display: "inline-block",
            padding: "20px 0",
        }
        this.uploadAadhar = this.uploadAadhar.bind(this);
        //this.uploadResume = this.uploadResume.bind(this);
        this.nextClicked = this.nextClicked.bind(this);
        this.showTerms = this.showTerms.bind(this);
        this.checkTerms = this.checkTerms.bind(this);
    }

    static getDerivedStateFromProps(props, state) {
        if (props.documentDetailError!=null && props.documentDetailError!= "") {
            return {
                ...state,
                spinnerClass: ["hide"]
            }
        }
        return state
    }

    showTerms() {
        if (this.state.termsVisible) {
            this.setState({
                termClass: ["hide"],
                termsVisible: false,
            })
        } else {
            this.setState({
                termClass: ["row"],
                termsVisible: true,
            })
        }
        
    }

    checkTerms() {
        this.setState({
            ...this.state,
            isTermsAccepted: !this.state.isTermsAccepted
        })
    }

    nextClicked(event) {
        event.preventDefault();
        this.setState({
            spinnerClass: [""]
        })
        this.props.documentUploaded(this.props.history)
    }

    uploadAadhar(event) {
        let file = event.target.files[0];
        var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
        if(!allowedExtensions.exec(file.name)){
            this.props.documentSubmitError("Govt. ID should be of .png/.jpg/.jpeg type only");
            return;
        }
        if (file) {
            let data = new FormData();
            data.append('file', file);
            this.props.aadharUploaded(data);
        }
    }

    // uploadResume(event) {
    //     let file = event.target.files[0];
    //     var allowedExtensions = /(\.pdf)$/i;
    //     if(!allowedExtensions.exec(file.name)){
    //         this.props.documentSubmitError("Resume should be of .pdf type only");
    //         return;
    //     }
    //     if (file) {
    //         let data = new FormData();
    //         data.append('file', file);
    //         this.props.resumeUploaded(data);
    //     }
    // }


    render() {
        return (
            <div className="container home-detail">
                <div className="section card z-depth-2 home-card hoverable">
        
                    <div className="row center">
                        <div className="col s12 m8 offset-m2">
                            <h4 className="header light">Document Details</h4>
                            <h6 className="header light">Upload your documents</h6>
                        </div>
                    </div>
                    <form onSubmit={this.nextClicked}>
                    
                    <div className="row">
                        <div className="input-field file-field col s9 offset-s1 m7 offset-m2">
                            <div className="btn">
                                <span>Your Govt. ID</span>
                                <input name="aadhar"
                                    type="file" 
                                    accept=".png, .jpeg, .jpg" 
                                    onChange={this.uploadAadhar}
                                    required/>
                            </div>
                            <div className="file-path-wrapper">
                                <input className="file-path validate" type="text" placeholder="(.png/.jpg)"/>
                            </div>
                            
                        </div>
                        <div className="col s1 m1" style={{paddingTop: "20px"}}>
                                { 
                                    this.props.isDocumentUploaded
                                    ? <i className="material-icons green-text">check</i>
                                    : null
                                }
                        </div>
                    </div>

                    {/* <div className="row">
                        <div className="input-field file-field col s12 m8 offset-m2">
                            <div className="btn">
                                <span>Resume</span>
                                <input  name="resume" 
                                    type="file" 
                                    accept=".pdf"
                                    onChange={this.uploadResume}
                                    required />
                            </div>
                            <div className="file-path-wrapper">
                                <input className="file-path validate" type="text" placeholder="(.pdf)"/>
                            </div>
                        </div>
                    </div> */}

                    <div className="row">
                        <div className="col s12 center-align">
                            <label>
                                <input type="hidden"/>
                                <span><a onClick={this.showTerms}>
                                    {
                                        this.state.termsVisible
                                        ? "Hide Terms"
                                        : "View Terms"
                                    }</a></span>
                            </label>
                        </div>

                        <div className={this.state.termClass.join(" ")} style={this.state.termsVisible? this.termStyle : null}>
                            <div className="col s10 offset-s1 m8 offset-m2">
                                <span>Students who do not meet the eligibility criteria as defined in 'Hiring Process' will be eliminated at any point of hiring process or as soon as EPAM identifies the student has provided misleading information deliberately.</span>
                            </div>
                        </div>
                    </div>

                    
                    <div className="row">
                        <div className="input-field col s10 offset-s1 m3 offset-m2">
                            <label>
                                <input type="checkbox" required checked={this.state.isTermsAccepted} onClick={this.checkTerms}/>
                                <span>Terms & Conditions</span>
                            </label>
                        </div>
                    </div>
                    
                    {
                        this.props.documentDetailError != null
                        ? <ErrorMessage message={this.props.documentDetailError}/>
                        : null
                    }

                    <div className="row center">
                        <Spinner className={this.state.spinnerClass.join(" ")} />
                    </div>
                    
                    <div className="row center">
                        <div className="input-field col s10 offset-s1 m4 offset-m2 left-align">
                            <button 
                                className="btn waves-effect waves-light" 
                                name="action"
                                onClick={() => this.props.changePath("/edit/location")}>Prev
                            <i className="material-icons left">chevron_left</i>
                            </button>
                        </div>
                        <div className="col s1 show-on-small hide-on-med-and-up"></div>
                        <div className="input-field col s10 offset-s1 m4 right-align">
                            <button className="btn waves-effect waves-light" type="submit" name="action">Finish
                            <i className="material-icons right">chevron_right</i>
                            </button>
                        </div>
                    </div>
                    </form>

                </div>


            </div>
        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        aadharUploaded: DocumentAction.aadharUploaded,
        //resumeUploaded: DocumentAction.resumeUploaded,
        documentUploaded: DocumentAction.documentUploaded,
        changePath: changePath,
        documentSubmitError: DocumentAction.documentSubmitError,
    }, dispatch)
)

const mapStateToProps = state => ({
    documentDetailError: state.appState.documentDetailError,
    isDocumentUploaded: state.appState.isDocumentUploaded,
})
export default connect(mapStateToProps, mapDispatchToProps)(DocumentDetail);