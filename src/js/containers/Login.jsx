import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Redirect, Link} from "react-router-dom";
import { loginEmailEntered, loginPasswordEntered, loginClicked} from "../actions/loginAction";
import { changePath } from "../actions/appStateAction";
import ErrorMessage from  "../components/ErrorMessage";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.loginClicked();
    }

    render() {
        return (
            <div className="container">
            {/* <div className="row center"> */}
            {/* <div className="col s12 l6 offset-l3"> */}
                
            
            <div className="section card z-depth-3 auth-card">
                <div className="row center">
                    <div className="col s12 m8 offset-m2">
                        <h4 className="header light">LOGIN</h4>
                    </div>
                </div>
                <form onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="input-field col s12 m8 offset-m2">
                        <i className="material-icons prefix">email</i>
                        <input 
                            id="emailID" 
                            type="email" 
                            className="validate" 
                            value={this.props.emailID} 
                            onChange={event => this.props.loginEmailEntered(event.target.value)}
                            required />
                        <label htmlFor="emailID">Email ID</label>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s12 m8 offset-m2">
                        <i className="material-icons prefix">lock</i>
                        <input id="password" 
                            type="password" 
                            className="validate" 
                            value={this.props.password} 
                            onChange={event => this.props.loginPasswordEntered(event.target.value)}
                            required />
                        <label htmlFor="password">Password</label>
                    </div>
                </div>
                {
                        this.props.loginError != null 
                        ? <ErrorMessage message={this.props.loginError}/>
                        : null
                }
                <div className="row center">
                    <div className="input-field col s12 m8 offset-m2">
                        <button className="btn waves-effect waves-light" type="submit" name="action">Login
                        <i className="material-icons right">send</i>
                        </button>
                    </div>
                </div>
                </form>
                <div className="row center">
                    <div className="input-field col s12 m8 offset-m2">
                        <span>
                            Not yet registered? 
                            <a onClick={() => this.props.changePath("/register")}> Register now</a>
                        </span>
                    </div>
                </div>

            </div>
            {/* </div> */}
            {/* </div> */}
            </div>
        )

        /*
        if (this.props.isLoggedIn) {
            return <Redirect to="/"/>
        }
        return (
            <div className="auth-content ">
                <form onSubmit={this.handleSubmit}>
                    <span className="auth-text">LOGIN</span>
                    <InputTag 
                        type="text"
                        placeholder="Email"
                        value={this.props.emailID}
                        onChange={event => this.props.loginEmailEntered(event.target.value)} />
                    <InputTag
                        type="password"
                        placeholder="Password"
                        value={this.props.password}
                        onChange={event => this.props.loginPasswordEntered(event.target.value)} />
                    <Button
                        type="submit"
                        label="LOGIN" />
                </form>
                <div className="auth-helper">
                    {
                        this.props.loginError != null 
                        ? <ErrorMessage message={this.props.loginError}/>
                        : null
                    }
                    <span>
                        Not yet registered? <Link to="/register">Register for an account</Link>
                    </span>
                </div>
            </div>
        )*/
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        loginEmailEntered: loginEmailEntered,
        loginPasswordEntered: loginPasswordEntered,
        loginClicked: loginClicked,
        changePath: changePath
    }, dispatch)
);

const mapStateToProps = state => ({
    emailID: state.login.emailId,
    password: state.login.password,
    isLoggedIn: state.appState.isLoggedIn,
    loginError: state.appState.loginError
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);