import { DOCUMENT_DETAIL_ACTION , APP_STATE_ACTION } from './actionType';
import { API } from "./../store/appState";
import axios from 'axios';
import { changePath } from './appStateAction';

export const aadharUploaded = aadharData => ({
    type: DOCUMENT_DETAIL_ACTION.AADHAR_UPLOADED,
    payload: aadharData
})

// export const resumeUploaded = resumeData => ({
//     type: DOCUMENT_DETAIL_ACTION.RESUME_UPLOADED,
//     payload: resumeData
// })

export const documentSubmitError = error => ({
    type: APP_STATE_ACTION.DOCUMENT_DATA_SUBMIT_ERROR,
    payload: error
})

export const documentUploaded = history => (dispatch, getState) => {
    dispatch({
        type: APP_STATE_ACTION.DOCUMENT_DATA_SUBMIT_ERROR,
        payload: ""
    })
    let status = true;
    let aadhar = getState().documentDetail.aadharData;
    //let resume = getState().documentDetail.resumeData;

    if (aadhar == null /*|| resume == null */) {
        dispatch({
            type: APP_STATE_ACTION.DOCUMENT_DATA_SUBMIT_ERROR,
            payload: "Invalid File Types"
        })
        return;
    }
    let aadharAPI = API + "/user/secure/file?id=" 
        + getState().appState.studentID
        + "&type=image";
    let resumeAPI = API + "/user/secure/file?id=" 
        + getState().appState.studentID
        + "&type=resume";
    let consentAPI = API + "/user/secure/consent?id="
        + getState().appState.studentID
   
    axios
        .post(aadharAPI, aadhar)
        .then((response) => {
            axios
                .post(consentAPI)
                .then(resposne=> {
                    if (status) {
                        dispatch({
                            type: APP_STATE_ACTION.DOCUMENT_UPLOADED,
                            payload: true
                        });
                        // history.push("/");
                        dispatch(changePath("/edit/status"));
                    }
                })
                .catch(error => {
                    dispatch(documentSubmitError(error))
                    status = false;
                })

            // axios.post(resumeAPI, resume)
            //     .then((response) => {
            //         //For Resume success
            //     })
            //     .catch(error => {
            //         dispatch(documentSubmitError(error))
            //         status = false;
            //     });

        })
        .catch(error => {
            dispatch(documentSubmitError(error))
            status = false;
        });
    
    
    
}


export const loadDocumentStatus = () => (dispatch, getState) => {
    const docStatusAPI = API + "/user/secure/fileStatus?id=" + getState().appState.studentID;
    axios.get(docStatusAPI)
        .then(response => response.data)
        .then(data => {
            dispatch({
                type: APP_STATE_ACTION.DOCUMENT_UPLOADED,
                payload: data
            })
        })
        .catch(error => {
            dispatch({
                type: APP_STATE_ACTION.DOCUMENT_UPLOADED,
                payload: false
            })
        })
}

export const DocumentAction = {
    //resumeUploaded: resumeUploaded,
    aadharUploaded: aadharUploaded,
    documentUploaded: documentUploaded,
    loadDocumentStatus: loadDocumentStatus,
    documentSubmitError: documentSubmitError
} 