import { ACADEMIC_DETAIL_ACTION, APP_STATE_ACTION} from "./actionType";
import { API } from "./../store/appState";
import axios from 'axios';
import { changePath } from "./appStateAction";

export const nameEntered = (name, id) => (dispatch, getState) => {
    let nameArray = getState().academicDetail.name;
    nameArray[id] = name
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.NAME_ENTERED,
        payload: nameArray,
    })
}

export const boardEntered = (board, id) => (dispatch, getState) => {
    let boardArray = getState().academicDetail.board;
    boardArray[id] = board;
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.BOARD_ENTERED,
        payload: boardArray
    })
}

export const graduationTypeEntered = (graduationType, id) => (dispatch, getState) => {
    let graduationTypeArray = getState().academicDetail.graduationType;
    graduationTypeArray[id] = graduationType;
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.GRADUATION_TYPE_ENTERED,
        payload: graduationTypeArray
    })
}


export const specializationTypeEntered = (specializationType, id) => (dispatch, getState) => {
    let specializationTypeArray = getState().academicDetail.specializationType;
    specializationTypeArray[id] = specializationType;
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.SPECIALIZATION_TYPE_ENTERED,
        payload: specializationTypeArray
    })
}


export const passOutYearEntered = (passOutYear, id) => (dispatch, getState) => {
    let passoutArray = getState().academicDetail.passOutYear;
    passoutArray[id] = passOutYear
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.PASS_OUT_YEAR_ENTERED,
        payload: passoutArray
    })
}

export const percentageOfMarksEntered = (percentageOfMarks, id) => (dispatch, getState) => {
    let percentageArray = getState().academicDetail.percentageOfMarks;
    percentageArray[id] = percentageOfMarks;
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.PERCENTAGE_OF_MARKS_ENTERED,
        payload: percentageArray
    })
}


export const isHighestEducationEntered = (isHighestEducation, id) => (dispatch, getState) => {
    let isHighestArray = getState().academicDetail.isHighestEducation;
    isHighestArray[id] = isHighestEducation;
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.IS_HIGHEST_EDUCATION_ENTERED,
        payload: isHighestArray
    })
}


export const deleteRecord =  id => (dispatch, getState) =>{
    let { name, board, graduationType, specializationType, passOutYear, percentageOfMarks, isHighestEducation } = getState().academicDetail;

    name.splice( id, 1 );
    board.splice( id, 1 );
    graduationType.splice(id, 1);
    specializationType.splice(id. i);
    passOutYear.splice( id, 1 );
    percentageOfMarks.splice( id, 1);
    isHighestEducation.splice( id, 1 );

    dispatch({
        type: ACADEMIC_DETAIL_ACTION.NAME_ENTERED,
        payload: name
    })
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.BOARD_ENTERED,
        payload: board
    })
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.GRADUATION_TYPE_ENTERED,
        payload: graduationType
    })
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.SPECIALIZATION_TYPE_ENTERED,
        payload: specializationType
    })
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.PASS_OUT_YEAR_ENTERED,
        payload: passOutYear
    })
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.PERCENTAGE_OF_MARKS_ENTERED,
        payload: percentageOfMarks
    })
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.IS_HIGHEST_EDUCATION_ENTERED,
        payload: isHighestEducation
    })

}

export const academicDetailSubmitted = (history) => (dispatch, getState) => {
    dispatch({
        type: APP_STATE_ACTION.ACADEMIC_DATA_SUBMIT_ERROR,
        payload: ""
    })

    let academicDetailAPI = API + "/user/secure/academicInfo";
    let addressData = [];
    for (let i=0; i<getState().academicDetail.board.length; i++) {
        let data = {
            userId: getState().appState.studentID,
            name: getState().academicDetail.name[i],
            board: getState().academicDetail.board[i],
            graduationType: getState().academicDetail.graduationType[i],
            specializationType: getState().academicDetail.specializationType[i],
            passOutYear: getState().academicDetail.passOutYear[i],
            percentageOfMarks: getState().academicDetail.percentageOfMarks[i],
            isHighestEducation: getState().academicDetail.isHighestEducation[i],
        }
        addressData.push(data);
    }

    axios.post(academicDetailAPI, addressData).then(response => response.data)
        .then(data => {
            if (data) {
                dispatch({
                    type: APP_STATE_ACTION.ACADEMIC_DATA_FILLED,
                    payload: true
                });
                dispatch(changePath("/edit/certificate"));
            }
        })
        .catch(error => {
            dispatch({
                type: APP_STATE_ACTION.ACADEMIC_DATA_SUBMIT_ERROR,
                payload: error
            })
        })
}


export const fillAcademicDetails = () => (dispatch, getState) => {
    let academicDataFetchAPI = API + "/user/secure/academicInfo?id=" + getState().appState.studentID;
    axios.get(academicDataFetchAPI)
        .then(response => response.data)
        .then(responseData => {
            if (responseData==null || responseData.length==0) {
                return;
            }
            let academicData = responseData;
            for(let i=0; i<academicData.length; i++) {

                let data = academicData[i];
                let id = i;//+1;
                dispatch(nameEntered(data.name, id));
                dispatch(boardEntered(data.board, id));
                dispatch(graduationTypeEntered(data.graduationType, id));
                dispatch(specializationTypeEntered(data.specializationType, id));
                dispatch(percentageOfMarksEntered(data.percentageOfMarks, id))
                dispatch(passOutYearEntered(data.passOutYear, id))
                dispatch(isHighestEducationEntered(data.isHighestEducation.toString(), id));
            }
            dispatch({
                type: APP_STATE_ACTION.ACADEMIC_DATA_FILLED,
                payload: true
            });
        });
}


export const Academic_Action = {
    nameEntered: nameEntered,
    boardEntered: boardEntered,
    graduationTypeEntered: graduationTypeEntered,
    specializationTypeEntered: specializationTypeEntered,
    passOutYearEntered: passOutYearEntered,
    percentageOfMarksEntered: percentageOfMarksEntered,
    isHighestEducationEntered: isHighestEducationEntered,
    academicDetailSubmitted: academicDetailSubmitted,
    fillAcademicDetails: fillAcademicDetails,
    deleteRecord: deleteRecord,
}