import { LOGIN_ACTION, APP_STATE_ACTION } from "./actionType";
import { changePath } from "./../actions/appStateAction";

export const loginEmailEntered = emailId => ({
    type: LOGIN_ACTION.EMAIL_ENTERED,
    payload: emailId
})

export const loginPasswordEntered = password => ({
    type: LOGIN_ACTION.PASSWORD_ENTERED,
    payload: password
})

export const loginClicked = () => (dispatch, getState) => {
    if (getState().login.emailId ==  getState().login.password) {
        dispatch({
            type: APP_STATE_ACTION.LOGIN_SUCCESS,
            payload: getState().login.emailId
        });
        dispatch(changePath("/edit/personal"));
    } else {
        dispatch({
            type: APP_STATE_ACTION.LOGIN_FAILED,
            payload: "Invalid Credentials"
        })
    }
}

export const logoutClicked = () => ({
    type: APP_STATE_ACTION.LOGOUT_CLICKED,
    payload: null
})