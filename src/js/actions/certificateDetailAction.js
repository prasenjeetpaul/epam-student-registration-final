import { CERTIFICATE_DETAIL_ACTION, APP_STATE_ACTION} from "./actionType";
import { API } from "./../store/appState";
import axios from 'axios';
import { changePath } from "./appStateAction";

export const nameEntered = (name, id) => (dispatch, getState) => {
    let nameArray = getState().certificateDetail.name;
    nameArray[id] = name
    dispatch({
        type: CERTIFICATE_DETAIL_ACTION.NAME_ENTERED,
        payload: nameArray,
    })
}

export const yearEntered = (year, id) => (dispatch, getState) => {
    let yearArray = getState().certificateDetail.year;
    yearArray[id] = year
    dispatch({
        type: CERTIFICATE_DETAIL_ACTION.YEAR_ENTERED,
        payload: yearArray,
    })
}

export const linkEntered = (link, id) => (dispatch, getState) => {
    let linkArray = getState().certificateDetail.link;
    linkArray[id] = link
    dispatch({
        type: CERTIFICATE_DETAIL_ACTION.LINK_ENTERED,
        payload: linkArray,
    })
}

export const deleteRecord = id => (dispatch, getState) => {
    let {name, year, link} = getState().certificateDetail;
    name.splice(id, 1);
    year.splice(id, 1);
    link.splice(id, 1);
    dispatch({
        type: CERTIFICATE_DETAIL_ACTION.NAME_ENTERED,
        payload: name
    })
    dispatch({
        type: CERTIFICATE_DETAIL_ACTION.YEAR_ENTERED,
        payload: year
    })
    dispatch({
        type: CERTIFICATE_DETAIL_ACTION.LINK_ENTERED,
        payload: link
    })
}


export const certificateDetailSubmitted = () => (dispatch, getState) => {
    dispatch({
        type: APP_STATE_ACTION.CERTIFICATE_DATA_SUBMIT_ERROR,
        payload: ""
    })
    let certificateAPI = API + "/user/secure/certificateDetails";
    let certificateData = [];
    for (let i=0; i<getState().certificateDetail.name.length; i++) {
        let data = {
            userId: getState().appState.studentID,
            name: getState().certificateDetail.name[i],
            year: getState().certificateDetail.year[i],
            link: getState().certificateDetail.link[i],
        }
        certificateData.push(data);
    }

    axios.post(certificateAPI, certificateData)
        .then(response => response.data)
        .then(data => {
            if (data) {
                dispatch({
                    type: APP_STATE_ACTION.CERTIFICATE_DATA_FILLED,
                    payload: true
                });
            }
            dispatch(changePath("/edit/location"));
        })
        .catch(error => {
            dispatch({
                type: APP_STATE_ACTION.CERTIFICATE_DATA_SUBMIT_ERROR,
                payload: error
            })
        })
}

export const fillCertificateDetails = () => (dispatch, getState) => {
    let certificateAPI = API + "/user/secure/certificateDetails?id=" + getState().appState.studentID;;
    axios.get(certificateAPI)
        .then(response => response.data)
        .then(responseData => {
            if (responseData==null || responseData.length==0) {
                return;
            }
            let certificateData = responseData;
            for(let i=0; i<certificateData.length; i++) {

                let data = certificateData[i];
                let id = i;
                dispatch(nameEntered(data.name, id));
                dispatch(yearEntered(data.year, id));
                dispatch(linkEntered(data.link, id))
            }
            dispatch({
                type: APP_STATE_ACTION.CERTIFICATE_DATA_FILLED,
                payload: true
            });
        });
}


export const CertificateAction = {
    nameEntered: nameEntered,
    yearEntered: yearEntered,
    linkEntered: linkEntered,
    certificateDetailSubmitted: certificateDetailSubmitted,
    deleteRecord: deleteRecord,
    fillCertificateDetails: fillCertificateDetails
}