import { PERSONAL_DETAIL_ACTION, APP_STATE_ACTION } from './actionType';
import { API } from "./../store/appState";
import { changePath } from "./appStateAction";
import axios from 'axios';
import M from "materialize-css";

// require ("babel-polyfill");


export const firstNameEntered = firstName => ({
    type: PERSONAL_DETAIL_ACTION.FIRST_NAME_ENTERED,
    payload: firstName
})

export const middleNameEntered = middleName => ({
    type: PERSONAL_DETAIL_ACTION.MIDDLE_NAME_ENTERED,
    payload: middleName
})

export const lastNameEntered = lastName => ({
    type: PERSONAL_DETAIL_ACTION.LAST_NAME_ENTERED,
    payload: lastName
})

export const dateOfBirthEntered = dateOfBirth => ({
    type: PERSONAL_DETAIL_ACTION.DATE_OF_BIRTH_ENTERED,
    payload: dateOfBirth
})

export const mobileNumberEntered = mobileNumber => ({
    type: PERSONAL_DETAIL_ACTION.MOBILE_NUMBER_ENTERED,
    payload: mobileNumber
})

export const genderEntered = gender => ({
    type: PERSONAL_DETAIL_ACTION.GENDER_ENTERED,
    payload: gender
})


export const fillPersonalDetails = () => (dispatch, getState) => {
    let personalDataFetchAPI = API + "/user/secure/personalInfo?id=" + getState().appState.studentID;
    axios.get(personalDataFetchAPI)  
        .then(response => response.data)
        .then(data => {
            if (data==null || data.length==0) {
                return;
            }
            //delete data.id;
            dispatch({
                type: PERSONAL_DETAIL_ACTION.PERSONAL_DATA_LOAD,
                payload: data,
            });
            dispatch({
                type: APP_STATE_ACTION.PEROSNAL_DATA_FILLED,
                payload: true
            });
            M.updateTextFields();
        });
}


export function fakeAPI() {
    return new Promise(resolve => {
        setTimeout(() => {
          resolve('resolved');
        }, 2000);
    });
}


const validatePersonalDetails = (dispatch, getState) => {
    if (getState().personalDetail.mobileNumber.length != 10) {
        dispatch({
            type: APP_STATE_ACTION.PERSONAL_DATA_SUBMIT_ERROR,
            payload: "Invalid Mobile Number (10 Digits Required)"
        })
        return false;
    }
    var namePattern = new RegExp("^[a-zA-Z]+$");

    if (!namePattern.test(getState().personalDetail.firstName) ) {
        dispatch({
            type: APP_STATE_ACTION.PERSONAL_DATA_SUBMIT_ERROR,
            payload: "Invalid First Name"
        })
        return false;
    }

    if (!namePattern.test(getState().personalDetail.lastName) ) {
        dispatch({
            type: APP_STATE_ACTION.PERSONAL_DATA_SUBMIT_ERROR,
            payload: "Invalid Last Name"
        })
        return false;
    }

    if (getState().personalDetail.middleName.length > 0 && !namePattern.test(getState().personalDetail.middleName) ) {
        dispatch({
            type: APP_STATE_ACTION.PERSONAL_DATA_SUBMIT_ERROR,
            payload: "Invalid Middle Name"
        })
        return false;
    }
    return true;
}


export const personalDetailSubmitted = (history) => (dispatch, getState) => {
    dispatch({
        type: APP_STATE_ACTION.PERSONAL_DATA_SUBMIT_ERROR,
        payload: ""
    })
    if (!validatePersonalDetails(dispatch, getState)) {
        return;
    }
    let personalDetailAPI = API + "/user/secure/personalInfo";
    axios.post(personalDetailAPI, {
            userId: getState().appState.studentID,
            ...getState().personalDetail
        })  .then(response => response.data)
            .then(data => {
                if (data) {
                    dispatch({
                        type: APP_STATE_ACTION.PEROSNAL_DATA_FILLED,
                        payload: true
                    });
                    dispatch(changePath("/edit/address"));
                }
            })
            .catch(error => {
                dispatch({
                    type: APP_STATE_ACTION.PERSONAL_DATA_SUBMIT_ERROR,
                    payload: error
                })
            })
        // history.push("/edit/address");
    ;

    // fakeAPI().then(data => {
    //     console.log("Submitted Data");
    //     console.log(getState().addressDetail);
    //     if (data != null) {
    //         dispatch({
    //             type: APP_STATE_ACTION.ADDRESS_DATA_FILLED,
    //             payload: true
    //         });
    //         dispatch(changePath("/edit/address"));
    //     }
    // });
}