export const LOGIN_ACTION = {
    EMAIL_ENTERED: "LOGIN_EMAIL_ENTERED",
    PASSWORD_ENTERED: "LOGIN_PASSWORD_ENTERED",
}

export const REGISTER_ACTION = {
    EMAIL_ENTERED: "REGISTER_EMAIL_ENTERED",
    PASSWORD1_ENTERED: "REGISTER_PASSWORD1_ENTERED",
    PASSWORD2_ENTERED: "REGISTER_PASSWORD2_ENTERED",
    CAPTCHA_ENTERED: "CAPTCHA_ENTERED",
}

export const PERSONAL_DETAIL_ACTION = {
    FIRST_NAME_ENTERED: "FIRST_NAME_ENTERED",
    MIDDLE_NAME_ENTERED: "MIDDLE_NAME_ENTERED",
    LAST_NAME_ENTERED: "LAST_NAME_ENTERED",
    DATE_OF_BIRTH_ENTERED: "DATE_OF_BIRTH_ENTERED",
    MOBILE_NUMBER_ENTERED: "MOBILE_NUMBER_ENTERED",
    GENDER_ENTERED: "GENDER_ENTERED",
    PERSONAL_DETAIL_SUBMITTED: "PERSONAL_DETAIL_SUBMITTED",

    PERSONAL_DATA_LOAD: "PERSONAL_DATA_LOAD",
}

export const ADDRESS_DETAIL_ACTION = {
    TYPE_ENTERED: "TYPE_ENTERED",
    HOUSE_NUMBER_ENTERED: "HOUSE_NUMBER_ENTERED",
    STREET_ENTERED: "STREET_ENTERED",
    LANDMARK_ENTERED: "LANDMARK_ENTERED",
    CITY_ENTERED: "CITY_ENTERED",
    STATE_ENTERED: "STATE_ENTERED",
    ZIPCODE_ENTERED: "ZIPCODE_ENTERED",
    COUNTRY_ENTERED: "COUNTRY_ENTERED",

    SELECTED_COUNTRY_ENTERED: "SELECTED_COUNTRY_ENTERED",
    SELECTED_STATE_ENTERED: "SELECTED_STATE_ENTERED",
    ADDRESS_DATA_LOAD: "ADDRESS_DATA_LOAD",
}

export const ACADEMIC_DETAIL_ACTION = {
    NAME_ENTERED: "NAME_ENTERED",
    BOARD_ENTERED: "BOARD_ENTERED",
    GRADUATION_TYPE_ENTERED: "GRADUATION_TYPE_ENTERED",
    SPECIALIZATION_TYPE_ENTERED: "SPECIALIZATION_TYPE_ENTERED",
    PASS_OUT_YEAR_ENTERED: "PASS_OUT_YEAR_ENTERED",
    PERCENTAGE_OF_MARKS_ENTERED: "PERCENTAGE_OF_MARKS_ENTERED",
    IS_HIGHEST_EDUCATION_ENTERED: "IS_HIGHEST_EDUCATION_ENTERED",

    ACADEMIC_DATA_LOAD: "ACADEMIC_DATA_LOAD",
}

export const CERTIFICATE_DETAIL_ACTION = {
    NAME_ENTERED: "CERTIFICATE_NAME_ENTERED",
    YEAR_ENTERED: "CERTIFICATE_YEAR_ENTERED",
    LINK_ENTERED: "CERTIFICATE_LINK_ENTERED"
}

export const LOCATION_DETAIL_ACTION = {
    JOB_LOCATION_LOADED: "JOB_LOCATION_LOADED",
    INTERVIEW_LOCATION_LOADED: "INTERVIEW_LOCATION_LOADED",

    JOB_PREFERENCE_ENTERED: "JOB_PREFERENCE_ENTERED",
    INTERVIEW_PREFERENCE_ENTERED: "INTERVIEW_PREFERENCE_ENTERED",
}

export const DOCUMENT_DETAIL_ACTION = {
    AADHAR_UPLOADED: "AADHAR_UPLOADED",
    RESUME_UPLOADED: "RESUME_UPLOADED",
}

export const APP_STATE_ACTION = {
    PATH_CHANGED: "PATH_CHANGED",
    
    LOGIN_SUCCESS: "LOGIN_SUCCESS",
    LOGOUT_CLICKED: "LOGOUT_CLICKED",
    LOGIN_FAILED: "LOGIN_FAILED",
    REGISTER_FAILED: "REGISTER_FAILED",
    REGISTER_SUCCESS: "REGISTER_SUCCESS",

    PEROSNAL_DATA_FILLED: "PEROSNAL_DATA_FILLED",
    ADDRESS_DATA_FILLED: "ADDRESS_DATA_FILLED",
    ACADEMIC_DATA_FILLED: "ACADEMIC_DATA_FILLED",
    CERTIFICATE_DATA_FILLED: "CERTIFICATE_DATA_FILLED",
    LOCATION_DATA_FILLED: "LOCATION_DATA_FILLED",
    DOCUMENT_UPLOADED: "DOCUMENT_UPLOADED",

    PERSONAL_DATA_SUBMIT_ERROR: "PERSONAL_DATA_SUBMIT_ERROR",
    ADDRESS_DATA_SUBMIT_ERROR: "ADDRESS_DATA_SUBMIT_ERROR",
    ACADEMIC_DATA_SUBMIT_ERROR: "ACADEMIC_DATA_SUBMIT_ERROR",
    CERTIFICATE_DATA_SUBMIT_ERROR: "CERTIFICATE_DATA_SUBMIT_ERROR",
    LOCATION_DATA_SUBMIT_ERROR: "LOCATION_DATA_SUBMIT_ERROR",
    DOCUMENT_DATA_SUBMIT_ERROR: "DOCUMENT_DATA_SUBMIT_ERROR",
}