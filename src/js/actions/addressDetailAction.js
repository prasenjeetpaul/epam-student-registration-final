import { ADDRESS_DETAIL_ACTION, APP_STATE_ACTION } from './actionType';
import { API } from "./../store/appState";
import { changePath } from "./appStateAction";
import axios from 'axios';

export const typeEntered = type => ({
    type: ADDRESS_DETAIL_ACTION.TYPE_ENTERED,
    payload: type
})

export const houseNumberEntered = houseNumber => ({
    type: ADDRESS_DETAIL_ACTION.HOUSE_NUMBER_ENTERED,
    payload: houseNumber
})

export const streetEntered = street => ({
    type: ADDRESS_DETAIL_ACTION.STREET_ENTERED,
    payload: street
})

export const landmarkEntered = landmark => ({
    type: ADDRESS_DETAIL_ACTION.LANDMARK_ENTERED,
    payload: landmark
})

export const cityEntered = city => ({
    type: ADDRESS_DETAIL_ACTION.CITY_ENTERED,
    payload: city
})

export const stateEntered = state => ({
    type: ADDRESS_DETAIL_ACTION.STATE_ENTERED,
    payload: state
})

export const zipcodeEntered = zipcode => ({
    type: ADDRESS_DETAIL_ACTION.ZIPCODE_ENTERED,
    payload: zipcode
})

export const countryEntered = country => ({
    type: ADDRESS_DETAIL_ACTION.COUNTRY_ENTERED,
    payload: country
})

export const selectedCountryEntered = countryName => ({
    type: ADDRESS_DETAIL_ACTION.SELECTED_COUNTRY_ENTERED,
    payload: countryName
})

export const selectedStateEntered = stateName => ({
    type: ADDRESS_DETAIL_ACTION.SELECTED_STATE_ENTERED,
    payload: stateName
})


export const fillAddressDetails = () => (dispatch, getState) => {
    let addressDataFetchAPI = API + "/user/secure/address?id=" + getState().appState.studentID;
    dispatch(fetchCountry());

    
    axios.get(addressDataFetchAPI)  
        .then(response => response.data)
        .then(data => {
            if (data.length == 0) {
                return;
            }
            
            //delete data[0].id;
            delete data[0].userId;
            dispatch(selectedCountryEntered(data[0].country))
            dispatch(fetchState(data[0].country));
            dispatch(selectedStateEntered(data[0].state))
            delete data[0].country;
            delete data[0].state;

            dispatch({
                type: ADDRESS_DETAIL_ACTION.ADDRESS_DATA_LOAD,
                payload: data[0],
            })
            
            dispatch({
                type: APP_STATE_ACTION.ADDRESS_DATA_FILLED,
                payload: true
            });
        });
}


export const fetchCountry = () => (dispatch, getState) => {
    let countryAPI = API + "/address/getCountries";
    axios.get(countryAPI)
        .then(response => response.data)
        .then(countryData => dispatch(countryEntered(countryData)));
};

export const fetchState = countryName => (dispatch, getState) => {
    dispatch(selectedCountryEntered(countryName))
    let stateAPI = API + "/address/getStates?country=" + countryName;
    axios.get(stateAPI)
        .then(resposne => resposne.data)
        .then(data => {
            dispatch(stateEntered(data));
        });
};

function cleanAddressData(addressData) {
    let cleanData = {
        ...addressData,
        type: "PERMANENT",
        state: addressData.selectedState,
        country: addressData.selectedCountry
    }
    delete cleanData.selectedCountry;
    delete cleanData.selectedState;
    return cleanData;
}

export const addressDetailSubmitted = (history) => (dispatch, getState) => {
    dispatch({
        type: APP_STATE_ACTION.ADDRESS_DATA_SUBMIT_ERROR,
        payload: ""
    })

    let addressDetailAPI = API + "/user/secure/address";
    axios.post(addressDetailAPI, [{
            userId: getState().appState.studentID,
            ...cleanAddressData(getState().addressDetail)
        }])  .then(response =>response.data)
            .then(data => {
                if (data) {
                    dispatch({
                        type: APP_STATE_ACTION.ADDRESS_DATA_FILLED,
                        payload: true
                    });
                    dispatch(changePath("/edit/academic"));
                }
            })
            .catch(error => {
                dispatch({
                    type: APP_STATE_ACTION.ADDRESS_DATA_SUBMIT_ERROR,
                    payload: error
                })
            })
       
    // }))
    // fakeAPI().then(data => {
    //     console.log("Submitted Data");
    //     console.log(getState().addressDetail);
    //     if (data != null) {
    //         dispatch({
    //             type: APP_STATE_ACTION.ADDRESS_DATA_FILLED,
    //             payload: true
    //         });
    //         history.push("/edit/academic");
    //     }
    // });
}

export const AddressAction = {
    typeEntered: typeEntered,
    houseNumberEntered: houseNumberEntered,
    streetEntered: streetEntered,
    landmarkEntered: landmarkEntered,
    cityEntered: cityEntered,
    stateEntered: stateEntered,
    zipcodeEntered: zipcodeEntered,
    countryEntered: countryEntered,
    addressDetailSubmitted: addressDetailSubmitted,
    
    fetchCountry: fetchCountry,
    fetchState: fetchState,

    fillAddressDetails: fillAddressDetails,
    
    selectedCountryEntered: selectedCountryEntered,
    selectedStateEntered: selectedStateEntered,
}