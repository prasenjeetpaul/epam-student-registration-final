import { REGISTER_ACTION, APP_STATE_ACTION } from "./actionType";
import { API } from "./../store/appState";
import { LoginAPI } from "../store/appState";
import axios from 'axios';
import M from "materialize-css";

export const registerEmailEntered = emailId => (dispatch, getState) => {
    dispatch({
        type: REGISTER_ACTION.EMAIL_ENTERED,
        payload: emailId
    })
    clearErrors(dispatch);
}


function testPassword(password) {
    var pattern = new RegExp("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*@#$%^&+=])(?=\\S+$).{8,}$");
    return pattern.test(password);
}

export const registerPassword1Entered = password => (dispatch, getState) => {
    dispatch({
        type: REGISTER_ACTION.PASSWORD1_ENTERED,
        payload: password
    })
    clearErrors(dispatch);
}

export const registerPassword2Entered = password => (dispatch, getState) => {
    dispatch({
        type: REGISTER_ACTION.PASSWORD2_ENTERED,
        payload: password
    })
    clearErrors(dispatch);
}

export const captchaEntered = captcha => (dispatch, getState) => {
    dispatch({
        type: REGISTER_ACTION.CAPTCHA_ENTERED,
        payload: captcha
    })
    clearErrors(dispatch);
}

const clearErrors = dispatch => {
    dispatch({
        type: APP_STATE_ACTION.REGISTER_FAILED,
        payload: null
    })
}

export const saveUserID = id => ({
    type: APP_STATE_ACTION.REGISTER_SUCCESS,
    payload: id
})

// export const saveAPI = API => ({
//     type: APP_STATE_ACTION.SAVE_API_LINK,
//     payload: API
// })

export const registerClicked = () => (dispatch, getState) => {
    if (getState().register.password2 != getState().register.password1) {
        dispatch({
            type: APP_STATE_ACTION.REGISTER_FAILED,
            payload: "Password didn't matched"
        })
    } else if (!testPassword(getState().register.password1)) {
        dispatch({
            type: APP_STATE_ACTION.REGISTER_FAILED,
            payload: "Invalid Password Format"
        })
        return;
    } else {
        let verifyEmail = API + "/verifyEmail";
        axios
            .post(verifyEmail, {
                emailId: window.btoa(getState().register.emailId)
            })  
            .then(response =>  {
                if(response.data) {
                    let registerUser = API + "/registration";
                    let registrationData = {
                        emailId: window.btoa(getState().register.emailId),
                        password: window.btoa(getState().register.password1),
                        captcha: window.btoa(getState().register.captcha),
                    }
                    axios
                        .post(registerUser, registrationData)  
                        .then(response => response.data)
                        .then(data => {
                            if (data == "false") {
                                dispatch({
                                    type: APP_STATE_ACTION.REGISTER_FAILED,
                                    payload: "Invalid Inputs"
                                })
                            } else if (data == "captchaError") {
                                dispatch({
                                    type: APP_STATE_ACTION.REGISTER_FAILED,
                                    payload: "Invalid Captcha"
                                })
                            } else {
                                // console.log(data);
                                //  M.toast({html: 'Registration Successful. Please click to Login', classes: 'rounded'});
                                //M.Modal({html:" Registration Testsfsfdsf", classes: 'rounded'})
                                if (confirm('Register Successfull') == true) {
                                    window.location.href = LoginAPI;
                                }
                                
                            M.updateTextFields();
                            }
                        })
                } else {
                    dispatch({
                        type: APP_STATE_ACTION.REGISTER_FAILED,
                        payload: "Email ID already exists"
                    })
                }
            })
            .catch(() => console.log("Can’t access the server. Blocked by browser!"))
    }
}