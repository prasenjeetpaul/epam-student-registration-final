import { LOCATION_DETAIL_ACTION, APP_STATE_ACTION} from "./actionType";
import { API } from "./../store/appState";
import axios from 'axios';
import { changePath } from "./appStateAction";

export const loadLocationData = () => (dispatch, getState) => {
    let jobLocationAPI = API + "/interviewDetails/jobLocations";
    let interviewLocationAPI = API + "/interviewDetails/interviewLocations";
    axios.get(jobLocationAPI)
        .then(response => response.data)
        .then(data => dispatch({
            type: LOCATION_DETAIL_ACTION.JOB_LOCATION_LOADED,
            payload: data
        }));
    
    axios.get(interviewLocationAPI)
        .then(response => response.data)
        .then(data => dispatch({
            type: LOCATION_DETAIL_ACTION.INTERVIEW_LOCATION_LOADED,
            payload: data
        }));
}

export const jobPreferenceEntered = (locationName, preferenceNumber) => (dispatch, getState) => {
    let jobPreferences = getState().locationDetail.jobLocationPreference;
    jobPreferences[preferenceNumber] = locationName;
    dispatch({
        type: LOCATION_DETAIL_ACTION.JOB_PREFERENCE_ENTERED,
        payload: jobPreferences
    })
}

export const interviewPreferenceEntered = (locationName, preferenceNumber) => (dispatch, getState) => {
    let interviewPreferences = getState().locationDetail.interviewLocationPreference;
    interviewPreferences[preferenceNumber] = locationName;
    dispatch({
        type: LOCATION_DETAIL_ACTION.INTERVIEW_PREFERENCE_ENTERED,
        payload: interviewPreferences
    })
}

export const locationDetailSubmitted = () => (dispatch, getState) => {
    dispatch({
        type: APP_STATE_ACTION.LOCATION_DATA_SUBMIT_ERROR,
        payload: ""
    })
    let locationAPI = API + "/user/secure/preferences";
    let locationData = {
        userId: getState().appState.studentID,
        interviewPreferences: getState().locationDetail.interviewLocationPreference,
        jobPreferences: getState().locationDetail.jobLocationPreference,
    };

    axios.post(locationAPI, locationData)
        .then(response => response.data)
        .then(data => {
            if (data) {
                dispatch({
                    type: APP_STATE_ACTION.LOCATION_DATA_FILLED,
                    payload: true
                });
                dispatch(changePath("/edit/document"));
            }
        })
        .catch(error => {
            dispatch({
                type: APP_STATE_ACTION.LOCATION_DATA_SUBMIT_ERROR,
                payload: error
            })
        })
}

export const fillLocationDetails = () => (dispatch, getState) =>  {
    let locationAPI = API + "/user/secure/preferences"+"?id=" + getState().appState.studentID;
    axios.get(locationAPI)
        .then(response => response.data)
        .then(data => {
            if (data==null || data.length==0) {
                return;
            }
            let isDataPresent = false;
            let { interviewPreferences, jobPreferences } = data;
            for (let jobPreference of jobPreferences) {
                if (jobPreference != null) {
                    isDataPresent = true;
                    break;
                }
            }
            if (!isDataPresent) {
                for (let interviewPreference of interviewPreferences) {
                    if (interviewPreference != null) {
                        isDataPresent = true;
                        break;
                    }
                }
            }
            dispatch({
                type: LOCATION_DETAIL_ACTION.INTERVIEW_PREFERENCE_ENTERED,
                payload: interviewPreferences
            })
            dispatch({
                type: LOCATION_DETAIL_ACTION.JOB_PREFERENCE_ENTERED,
                payload: jobPreferences
            })
            dispatch({
                type: APP_STATE_ACTION.LOCATION_DATA_FILLED,
                payload: isDataPresent
            })
        })
}

export const LocationAction = {
    loadLocationData: loadLocationData,
    jobPreferenceEntered: jobPreferenceEntered,
    interviewPreferenceEntered: interviewPreferenceEntered,
    locationDetailSubmitted: locationDetailSubmitted,
    fillLocationDetails: fillLocationDetails
}