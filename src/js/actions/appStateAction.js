import { APP_STATE_ACTION } from "./actionType";

export const changePath = path => ({
    type: APP_STATE_ACTION.PATH_CHANGED,
    payload: path
})

